SUMMARY
=======
This is the DuMuX module containing the code for producing the results for the Forschungsmodul 1 "Extension of a locally-refined quadtree finite-volume staggered-grid scheme to a drop-on-wall geometry" of Ivan Buntic.

Installation with Docker

Create a new directory:
```
mkdir New_Folder
cd New_folder
```

Download the Docker Image
```
wget https://git.iws.uni-stuttgart.de/dumux-pub/buntic2020a/-/raw/master/docker/pubtable_buntic2020a
```

Open the Docker Image

```
bash pubtable_buntic2020a open
```
Run a Simulation
```
cd Buntic2020a/build-cmake/appl/freeflow/navierstokes
make test_droplet_FM
./test_droplet_FM test_droplet_FM.input

```


Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder
cd New_folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/buntic2020a.git
```

After that, execute the file [installBuntic2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/buntic2020a/-/blob/master/installBuntic2020a.sh)

```
chmod u+x buntic2020a/installBuntic2020a.sh
./buntic2020a/installBuntic2020a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Applications
============


__Building from source__:

In order run the simulations of the different cases navigate to the folder

```bash
cd buntic2020a/build-cmake/appl/freeflow/navierstokes
```

Compile the programm:

```bash
make test_droplet_FM
```

And run by:

* ```./test_droplet_FM test_droplet_FM.input```
