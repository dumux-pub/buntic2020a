// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_TEST_PROBLEM_HH
#define DUMUX_CHANNEL_TEST_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/assembly/staggeredrefinedlocalresidual.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/myfluxvariables.hh>
#include <dumux/freeflow/navierstokes/myproblem.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/mylocalresidual.hh>
#include <dumux/freeflow/navierstokes/myvolumevariables.hh>
#include <dumux/freeflow/navierstokes/myiofields.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/discretization/staggered/freeflow/myfacevariables.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>
#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/myfacesolution.hh>
#include <dumux/discretization/staggered/mygridvariables.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>

namespace Dumux
{
template <class TypeTag>
class ChannelTestProblem;

namespace Properties
{
#if !NONISOTHERMAL
// Create new type tags
namespace TTag {
struct ChannelTestTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag
#else
struct ChannelTestTypeTag { using InheritsFrom = std::tuple<NavierStokesNI, StaggeredFreeFlowModel>; };
} // end namespace TTag
#endif
// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ChannelTestTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
#if NONISOTHERMAL
    using type = FluidSystems::OnePLiquid<Scalar, Components::SimpleH2O<Scalar> >;
#else
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
#endif
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ChannelTestTypeTag> { using type = Dumux::ChannelTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::ChannelTestTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::ChannelTestTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::ChannelTestTypeTag> { static constexpr bool value = true; };

//! The variables living on the faces
template<class TypeTag>
struct FaceVariables<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
public:
    using type = MyStaggeredFaceVariables<ModelTraits, FacePrimaryVariables, GridView::dimension, upwindSchemeOrder>;
};

//! The velocity output
template<class TypeTag>
struct VelocityOutput<TypeTag, TTag::ChannelTestTypeTag>
{
    using type = MyStaggeredFreeFlowVelocityOutput<GetPropType<TypeTag, Properties::GridVariables>,
                                                 GetPropType<TypeTag, Properties::SolutionVector>>;
};

template<class TypeTag, class MyTypeTag>
struct FaceVolumeVariables { using type = UndefinedProperty; };                     //!< The secondary variables within a sub-control volume

//! Set the volume variables property
template<class TypeTag>
struct FaceVolumeVariables<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    using VV = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    static constexpr auto dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using type = NavierStokesFaceVolumeVariables<VV, numPairs>;
};

//! Set the default global volume variables cache vector class
template<class TypeTag>
struct GridVolumeVariables<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FaceVolumeVariables = GetPropType<TypeTag, Properties::FaceVolumeVariables>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using SubControlVolume = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolume;
    using SubControlVolumeFace = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolumeFace;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridVolumeVariablesCache>();
    using Traits = MyStaggeredGridDefaultGridVolumeVariablesTraits<Problem, FaceVolumeVariables, VolumeVariables, GridView, SubControlVolume, SubControlVolumeFace>;
public:
    using type = MyStaggeredGridVolumeVariables<Traits, enableCache>;
};

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::ChannelTestTypeTag> { using type = StaggeredRefinedLocalResidual<TypeTag>; };

//! Set the face solution type
template<class TypeTag>
struct StaggeredFaceSolution<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
public:
    using type = Dumux::MyStaggeredFaceSolution<FaceSolutionVector>;
};

//! Set the grid variables (volume, flux and face variables)
template<class TypeTag>
struct GridVariables<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    using GG = GetPropType<TypeTag, Properties::GridGeometry>;
    using GVV = GetPropType<TypeTag, Properties::GridVolumeVariables>;
    using GFVC = GetPropType<TypeTag, Properties::GridFluxVariablesCache>;
    using GFV = GetPropType<TypeTag, Properties::GridFaceVariables>;
public:
    using type = MyStaggeredGridVariables<GG, GVV, GFVC, GFV>;
};

//! The default fv grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;

    static_assert(FSY::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid system");
    static_assert(FST::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid state");
    static_assert(!FSY::isMiscible(), "The Navier-Stokes model only works with immiscible fluid systems.");

    using Traits = NavierStokesVolumeVariablesTraits<PV, FSY, FST, MT>;
public:
    using type = MyNavierStokesVolumeVariables<Traits>;
};

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::ChannelTestTypeTag> { using type = RefinedNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::ChannelTestTypeTag> { using type = RefinedNavierStokesFluxVariables<TypeTag>; };

//! The specific I/O fields
template<class TypeTag>
struct IOFields<TypeTag, TTag::ChannelTestTypeTag> { using type = MyNavierStokesIOFields; };
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the one-phase (Navier-) Stokes problem in a channel.
 * \todo doc me!
 */
template <class TypeTag>
class ChannelTestProblem : public MyNavierStokesProblem<TypeTag>
{
    using ParentType = MyNavierStokesProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    static constexpr auto dimWorld = GetPropType<TypeTag, Properties::GridView>::dimensionworld;

    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using TimeLoopPtr = std::shared_ptr<CheckPointTimeLoop<Scalar>>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;

public:
    ChannelTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6)
    {
        inletVelocity_ = getParam<Scalar>("Problem.InletVelocity");
    }

   /*!
     * \name Problem parameters
     */
    // \{


    bool shouldWriteRestartFile() const
    {
        return false;
    }

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }
    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        if(isInlet(globalPos))
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
#if NONISOTHERMAL
            values.setDirichlet(Indices::temperatureIdx);
#endif
        }
        else if(isOutlet(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
#if NONISOTHERMAL
            values.setOutflow(Indices::energyBalanceIdx);
#endif
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
#if NONISOTHERMAL
            values.setNeumann(Indices::energyBalanceIdx);
#endif
        }

        return values;
    }

    /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    PrimaryVariables analyticalSolution(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;

        values[Indices::pressureIdx] = 1.1e+5;
        values[Indices::velocityXIdx] = inletVelocity_*globalPos[1]/this->gridGeometry().bBoxMax()[1];
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void createAnalyticalSolution()
    {
        analyticalPressure_.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocity_.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocityOnFace_.resize(this->gridGeometry().numFaceDofs());

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();
                auto ccDofPosition = scv.dofPosition();
                auto analyticalSolutionAtCc = analyticalSolution(ccDofPosition);

                // velocities on faces
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    const auto faceDofIdx = scvf.dofIndex();
                    const auto faceDofPosition = scvf.center();
                    const auto dirIdx = scvf.directionIndex();
                    const auto analyticalSolutionAtFace = analyticalSolution(faceDofPosition);
                    analyticalVelocityOnFace_[faceDofIdx][dirIdx] = analyticalSolutionAtFace[Indices::velocity(dirIdx)];
                }

                analyticalPressure_[ccDofIdx] = analyticalSolutionAtCc[Indices::pressureIdx];

                for(int dirIdx = 0; dirIdx < ModelTraits::dim(); ++dirIdx)
                    analyticalVelocity_[ccDofIdx][dirIdx] = analyticalSolutionAtCc[Indices::velocity(dirIdx)];
            }
        }
    }

   /*!
     * \brief Returns the analytical solution for the pressure
     */
    auto& getAnalyticalPressureSolution() const
    {
        return analyticalPressure_;
    }

   /*!
     * \brief Returns the analytical solution for the velocity
     */
    auto& getAnalyticalVelocitySolution() const
    {
        return analyticalVelocity_;
    }

   /*!
     * \brief Returns the analytical solution for the velocity at the faces
     */
    auto& getAnalyticalVelocitySolutionOnFace() const
    {
        return analyticalVelocityOnFace_;
    }

   /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values ;

        values[Indices::velocityXIdx] = 0.;
        values[Indices::velocityYIdx] = 0.;
        values[Indices::pressureIdx] = 1.1e+5;

        if(isInlet(globalPos))
        {
            values[Indices::velocityXIdx] = inletVelocity_;
#if NONISOTHERMAL
        // give the system some time so that the pressure can equilibrate, then start the injection of the hot liquid
        if(time() >= 200.0)
            values[Indices::temperatureIdx] = 293.15;
#endif
        }

        return values;
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1.1e+5;
        values[Indices::velocityXIdx] = 0.;
        values[Indices::velocityYIdx] = 0.;

#if NONISOTHERMAL
        values[Indices::temperatureIdx] = 283.15;
#endif

        return values;
    }

    // \}
    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
        if(inletVelocity_ > eps_)
            timeLoop_->setCheckPoint({200.0, 210.0});
    }

    Scalar time() const
    {
        return timeLoop_->time();
    }

private:
    bool isInlet(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < eps_;
    }

    bool isOutlet(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > 10-eps_;
    }

    Scalar eps_;
    Scalar inletVelocity_;
    TimeLoopPtr timeLoop_;
    std::vector<Scalar> analyticalPressure_;
    std::vector<VelocityVector> analyticalVelocity_;
    std::vector<VelocityVector> analyticalVelocityOnFace_;
};
} //end namespace

#endif
