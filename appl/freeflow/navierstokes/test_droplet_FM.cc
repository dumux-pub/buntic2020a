// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */

 #include <config.h>

 #include <ctime>
 #include <iostream>

 #include <dune/common/parallel/mpihelper.hh>
 #include <dune/common/timer.hh>
 #include <dune/grid/io/file/dgfparser/dgfexception.hh>
 #include <dune/grid/io/file/vtk.hh>
 #include <dune/istl/io.hh>

#include "droplet_FM.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/subgridgridcreator.hh>

#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

namespace Dumux
{
namespace Properties
{
// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using type = Dune::SubGrid<dim, HostGrid>;
};
}}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::ChannelTestTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    // The hostgrid
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;/*typename GetPropType<TypeTag, Properties::Grid>::HostGrid;*/
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    const std::vector<int> numCells = getParam<std::vector<int>>("Grid.Cells");
    const std::vector<double> geomSize = getParam<std::vector<double>>("Grid.UpperRight");

    const std::vector<double> centerLarge = getParam<std::vector<double>>("Grid.centerLarge");
    const std::vector<double> centerSmall = getParam<std::vector<double>>("Grid.centerSmall");

    const double radiusLarge = getParam<double>("Grid.radiusLarge");
    const double radiusSmall = getParam<double>("Grid.radiusSmall");

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const Scalar cellSizeX = geomSize[0] / numCells[0];
    const Scalar cellSizeY = geomSize[1] / numCells[1];
    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridViewVar = hostGrid.leafGridView();

    hostGrid.preAdapt();




    //if radiusLarge > radiusSmall: large drop on the left, small one on the right

    /*

    for (const auto& element : elements(leafGridViewVar))
    {
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        GlobalPosition pos = element.geometry().center();

        auto x = pos[0];
        auto y = pos[1];

        double diag = 1.0*cellSizeX;

        if(((x - centerLarge[0])*(x - centerLarge[0]) + (y - centerLarge[1])*(y - centerLarge[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
        {
            hostGrid.mark( 1,  element);
        }


        if(((x - centerSmall[0])*(x - centerSmall[0]) + (y - centerSmall[1])*(y - centerSmall[1])) <= (radiusSmall+diag)*(radiusSmall+diag))
        {
            hostGrid.mark( 1,  element);
        }

    }

    hostGrid.adapt();
    hostGrid.postAdapt();

    for (const auto& element : elements(leafGridViewVar))
    {
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        GlobalPosition pos = element.geometry().center();

        auto x = pos[0];
        auto y = pos[1];

        double diag = 0.7*cellSizeX;

        if(((x - centerLarge[0])*(x - centerLarge[0]) + (y - centerLarge[1])*(y - centerLarge[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
        {
            hostGrid.mark( 1,  element);
        }


        if(((x - centerSmall[0])*(x - centerSmall[0]) + (y - centerSmall[1])*(y - centerSmall[1])) <= (radiusSmall+diag)*(radiusSmall+diag))
        {
            hostGrid.mark( 1,  element);
        }
    }

    hostGrid.adapt();
    hostGrid.postAdapt();

    // The subgrid
    auto selector = [](const auto& element)
    {   //model the drop as a half-circle
        const std::vector<double> centerLarge = getParam<std::vector<double>>("Grid.centerLarge");
        const std::vector<double> centerSmall = getParam<std::vector<double>>("Grid.centerSmall");

        const double radiusLarge = getParam<double>("Grid.radiusLarge");
        const double radiusSmall = getParam<double>("Grid.radiusSmall");

        bool checker = false;

        if(((element.geometry().center()[0] - centerLarge[0])*(element.geometry().center()[0] - centerLarge[0]) + (element.geometry().center()[1] - centerLarge[1])*(element.geometry().center()[1] - centerLarge[1]) >= radiusLarge*radiusLarge) &&
            ((element.geometry().center()[0] - centerSmall[0])*(element.geometry().center()[0] - centerSmall[0]) + (element.geometry().center()[1] - centerSmall[1])*(element.geometry().center()[1] - centerSmall[1]) >= radiusSmall*radiusSmall))
        {
            checker = true;
        }

        return checker;
    };

  */






    //if radiusLarge > radiusSmall: large drop on the right, small one on the left

    /*

    for (const auto& element : elements(leafGridViewVar))
    {
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        GlobalPosition pos = element.geometry().center();

        auto x = pos[0];
        auto y = pos[1];

        double diag = 1.0*cellSizeX;

        if(((x - centerSmall[0])*(x - centerSmall[0]) + (y - centerSmall[1])*(y - centerSmall[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
        {
            hostGrid.mark( 1,  element);
        }

        if(((x - centerLarge[0])*(x - centerLarge[0]) + (y - centerLarge[1])*(y - centerLarge[1])) <= (radiusSmall+diag)*(radiusSmall+diag))
        {
            hostGrid.mark( 1,  element);
        }

    }

    hostGrid.adapt();
    hostGrid.postAdapt();

    for (const auto& element : elements(leafGridViewVar))
    {
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        GlobalPosition pos = element.geometry().center();

        auto x = pos[0];
        auto y = pos[1];

        double diag = 0.7*cellSizeX;

        if(((x - centerSmall[0])*(x - centerSmall[0]) + (y - centerSmall[1])*(y - centerSmall[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
        {
            hostGrid.mark( 1,  element);
        }


        if(((x - centerLarge[0])*(x - centerLarge[0]) + (y - centerLarge[1])*(y - centerLarge[1])) <= (radiusSmall+diag)*(radiusSmall+diag))
        {
            hostGrid.mark( 1,  element);
        }
    }

    hostGrid.adapt();
    hostGrid.postAdapt();

    // The subgrid
    auto selector = [](const auto& element)
    {   //model the drop as a half-circle
        const std::vector<double> centerLarge = getParam<std::vector<double>>("Grid.centerLarge");
        const std::vector<double> centerSmall = getParam<std::vector<double>>("Grid.centerSmall");

        const double radiusLarge = getParam<double>("Grid.radiusLarge");
        const double radiusSmall = getParam<double>("Grid.radiusSmall");

        bool checker = false;

        if(((element.geometry().center()[0] - centerLarge[0])*(element.geometry().center()[0] - centerLarge[0]) + (element.geometry().center()[1] - centerLarge[1])*(element.geometry().center()[1] - centerLarge[1]) >= radiusSmall*radiusSmall) &&
            ((element.geometry().center()[0] - centerSmall[0])*(element.geometry().center()[0] - centerSmall[0]) + (element.geometry().center()[1] - centerSmall[1])*(element.geometry().center()[1] - centerSmall[1]) >= radiusLarge*radiusLarge))
        {
            checker = true;
        }

        return checker;
    };

    */




    //if radiusLarge == radiusSmall
    // /*
     for (const auto& element : elements(leafGridViewVar))
     {
            using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
            using GridView = typename GridGeometry::GridView;
            using Element = typename GridView::template Codim<0>::Entity;
            using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
            GlobalPosition pos = element.geometry().center();

            auto x = pos[0];
            auto y = pos[1];

            double diag = 1.0*cellSizeX;

            if(((x - centerSmall[0])*(x - centerSmall[0]) + (y - centerSmall[1])*(y - centerSmall[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
            {
                hostGrid.mark( 1,  element);
            }

            if(((x - centerLarge[0])*(x - centerLarge[0]) + (y - centerLarge[1])*(y - centerLarge[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
            {
                hostGrid.mark( 1,  element);
            }

    }

    hostGrid.adapt();
    hostGrid.postAdapt();

    for (const auto& element : elements(leafGridViewVar))
    {
        using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
        using GridView = typename GridGeometry::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        GlobalPosition pos = element.geometry().center();

        auto x = pos[0];
        auto y = pos[1];

        double diag = 0.7*cellSizeX;

        if(((x - centerSmall[0])*(x - centerSmall[0]) + (y - centerSmall[1])*(y - centerSmall[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
        {
            hostGrid.mark( 1,  element);
        }


        if(((x - centerLarge[0])*(x - centerLarge[0]) + (y - centerLarge[1])*(y - centerLarge[1])) <= (radiusLarge+diag)*(radiusLarge+diag))
        {
            hostGrid.mark( 1,  element);
        }
    }

    hostGrid.adapt();
    hostGrid.postAdapt();

    // The subgrid
    auto selector = [](const auto& element)
    {   //model the drop as a half-circle
        const std::vector<double> centerLarge = getParam<std::vector<double>>("Grid.centerLarge");
        const std::vector<double> centerSmall = getParam<std::vector<double>>("Grid.centerSmall");

        const double radiusLarge = getParam<double>("Grid.radiusLarge");
        const double radiusSmall = getParam<double>("Grid.radiusSmall");

        bool checker = false;

        if(((element.geometry().center()[0] - centerLarge[0])*(element.geometry().center()[0] - centerLarge[0]) + (element.geometry().center()[1] - centerLarge[1])*(element.geometry().center()[1] - centerLarge[1]) >= radiusLarge*radiusLarge) &&
            ((element.geometry().center()[0] - centerSmall[0])*(element.geometry().center()[0] - centerSmall[0]) + (element.geometry().center()[1] - centerSmall[1])*(element.geometry().center()[1] - centerSmall[1]) >= radiusLarge*radiusLarge))
        {
            checker = true;
        }

        return checker;
    };

//      */




    auto subGridPtr = SubgridGridCreator<HostGrid>::makeGrid(hostGrid, selector);

    subGridPtr->setMaxLevelDifference(2);

    // we compute on the leaf grid view
    const auto& leafGridView = subGridPtr->leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = (*gridGeometry).numIntersections();
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);
    problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x, xOld);

    problem->createAnalyticalSolution();

    // initialize the vtk output module
    using VtkOutputFields = GetPropType<TypeTag, Properties::VtkOutputFields>;
    MyStaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    VtkOutputFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.addField(problem->getAnalyticalPressureSolution(), "pressureExact");
    vtkWriter.addField(problem->getAnalyticalVelocitySolution(), "velocityExact");
    vtkWriter.addFaceField(problem->getAnalyticalVelocitySolutionOnFace(), "faceVelocityExact");
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // solve the non-linear system with time step control
    nonLinearSolver.solve(x);

    // write vtk output
    vtkWriter.write(1.0);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
