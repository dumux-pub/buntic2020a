# readme for the dumux pub table Buntic2020a

You created a Docker image buntic2020a. Next steps:

* Try your container by running pubtable_buntic2020a open
  See below for instructions how to share files with the host system.

* Push the docker image to DockerHub or the GitLab Docker registry of your dumux-pub module.
  Look at the Registry tab of your dumux-pub module for help.

* Replace the image name in pubtable_buntic2020a with the actual image name
  e.g. git.iws.uni-stuttgart.de:4567/dumux-pub/koch2017a.

* [Optional] Add the Dockerfile to the git repository.

* Add the pubtable_buntic2020a script to the git repository (this is for the user)
  and add the following lines to your README.md:

Using the pub table Buntic2020a with docker
=============================================

In order to run simulations of this pub table look
at the convenience script pubtable_buntic2020a.
First download the script from the git repository.

The simplest way is to spin up a container
is creating a new folder "dumux"
$ mkdir dumux
change to the new folder
$ cd dumux
and open the pub table by running
$ pubtable_buntic2020a open

The container will spin up. It will mount the "dumux"
directory into the container at /dumux/shared. Put files
in this folder to share them with the host machine. This
could be e.g. VTK files produced by the simulation that
you want to visualize on the host machine.

