// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyMyFreeFlowStaggeredGeometryHelper
 */
#ifndef DUMUX_DISCRETIZATION_MY_STAGGERED_GEOMETRY_HELPER_HH
#define DUMUX_DISCRETIZATION_MY_STAGGERED_GEOMETRY_HELPER_HH

#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/referenceelements.hh>

#include <dumux/common/math.hh>
#include <type_traits>
#include <algorithm>

#include <dumux/discretization/staggered/freeflow/staggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/freegeometryfunctions.hh>

namespace Dumux
{
/*!
 * \ingroup StaggeredDiscretization
 * \brief Parallel Data stored per sub face
 */
template<class Scalar, class GlobalPosition>
struct MyPairData
{
    std::vector<std::vector<int>> parallelDofs;
    std::vector<std::vector<Scalar>> parallelDofsInterpolationFactors;
    std::vector<Scalar> parallelDistances;
//     --------------
//     | o parallel2|
//     | o         -->
//     | o          |
//     --------------
//     | * parallel1|
//     | *         -->
//     | *          |
//     --------------
//     | +     self||
//     | +         -->
//     | +         ||
//     --------------
//
//     +: parallelDistances[0]
//     *: parallelDistances[1]
//     o: parallelDistances[2]
    std::pair<std::vector<signed int>,std::vector<signed int>> normalPair;
    std::pair<std::vector<Scalar>,std::vector<Scalar>> normalPairInterpolationFactors;
    std::vector<int> localNormalFaceIndices; //local indices of all normal intersections contributing to the interpolated normal velocity value
    int localNormalFluxCorrectionIndex; //local index of the normal intersection touching the intersection_
    Scalar normalDistance; //distance between outer normal dof position (second) and inner normal dof position (first) of a certain pair
    GlobalPosition virtualFirstParallelFaceDofPos;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief In Axis Data stored per sub face
 */
template<class Scalar>
struct MyAxisData
{
    int selfDof;
    std::vector<int> oppositeDofs;
    std::vector<Scalar> oppositeDofsInterpolationFactors;
    std::vector<int> inAxisForwardDofs;
    std::vector<int> inAxisBackwardDofs;
    Scalar selfToOppositeDistance;
    std::vector<Scalar> inAxisForwardDistances;
    std::vector<Scalar> inAxisBackwardDistances;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Volume variables data stored per sub face
 */
template<class Scalar>
struct MyVolVarsData
{
//                     |   outer   |
//                     |   vol     |
//                     |   vars    |
//                     -------------
//                     |           ||in
//                     | innner    ||ter
//                     | vol       ||sec
//                     | vars      ||tion_
//                     |           ||
//                     -------------
//                     |   outer   |
//                     |   vol     |
//                     |   vars    |

    //inner is the reference staggered element, outer are neighbors which touch in a normalFace
    std::vector<int> innerVolVarsDofs;
    std::vector<Scalar> innerVolVarsDofsInterpolationFactors;
    std::vector<int> outerVolVarsDofs;
    std::vector<Scalar> outerVolVarsDofsInterpolationFactors;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class constructing the dual grid finite volume geometries
 *        for the free flow staggered discretization method.
 */
template<class GridView, class IntersectionMapper>
class MyMyFreeFlowStaggeredGeometryHelper
{
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim-1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;

    //TODO include assert that checks for quad geometry
    static constexpr int numPairs = 2 * (dimWorld - 1);
    static int order_;

public:
    using PairData = MyPairData<Scalar, GlobalPosition>;
    using AxisData = MyAxisData<Scalar>;

    MyMyFreeFlowStaggeredGeometryHelper(const Element& element, const GridView& gridView, const IntersectionMapper& intersectionMapper) : element_(element), elementGeometry_(element.geometry()), gridView_(gridView), intersectionMapper_(intersectionMapper)
    { }

    //! update the local face
    void updateLocalFace(const Intersection& intersection)
    {
        intersection_ = intersection;

        fillPairData_();
        fillAxisData_();
        fillVolVarsData_();

//         std::cout << std::endl << "element center = " << element_.geometry().center() << ", intersection center = " << intersection_.geometry().center() << std::endl;
//         for (unsigned int i = 0; i < numPairs; ++i)
//         {
//             std::cout << "pairData of pair " << i << ":" << std::endl;
//
//             std::cout << "parallel dofs = ";
//             for (const auto& prallelDof : pairData_[i].parallelDofs[0/*order*/])
//             {
//                     std::cout << prallelDof << ", ";
//             }
//             std::cout << std::endl;
//
//             std::cout << "parallelSelfDistance = " << pairData_[i].parallelDistances[0] << std::endl;
//             std::cout << "parallelNextDistance = " << pairData_[i].parallelDistances[1] << std::endl;
//
//             std::cout << "normalfirst = ";
//             for (const auto& normalFirstDof : pairData_[i].normalPair.first )
//             {
//                 std::cout << normalFirstDof << ", ";
//             }
//             std::cout << std::endl;
//
//             std::cout << "normalsecond = ";
//             for (const auto& normalSecondDof : pairData_[i].normalPair.second )
//             {
//                 std::cout << normalSecondDof << ", ";
//             }
//             std::cout << std::endl;
//
//             std::cout << "localNormalFaceIndices = ";
//             for (const auto& localNormalFaceIdx: pairData_[i].localNormalFaceIndices)
//             {
//                 std::cout << localNormalFaceIdx << ", ";
//             }
//             std::cout << std::endl;
//
//             std::cout << "localNormalFluxCorrectionIndex = " << pairData_[i].localNormalFluxCorrectionIndex << std::endl;
//
//             std::cout << "normalDistance = " << pairData_[i].normalDistance << std::endl;
//
//             std::cout << "virtualFirstParallelFaceDofPos = " << pairData_[i].virtualFirstParallelFaceDofPos << std::endl;
//         }
//
//         std::cout << "axis data:" << std::endl;
//
//         std::cout << "self dof = " << axisData_.selfDof << std::endl;
//
//         std::cout << "opposite dofs = ";
//         for (const auto& oppositeDof : axisData_.oppositeDofs)
//         {
//             std::cout << oppositeDof << ", ";
//         }
//         std::cout << std::endl;
//
//         std::cout << "inAxisForwardDofs = " ;
//         for (const auto& inAxisForwardDof : axisData_.inAxisForwardDofs){
//             std::cout << inAxisForwardDof << ", ";
//         }
//         std::cout << std::endl;
//
//         std::cout << "inAxisBackwardDofs = " ;
//         for (const auto& inAxisBackwardDof : axisData_.inAxisBackwardDofs){
//             std::cout << inAxisBackwardDof << ", ";
//         }
//         std::cout << std::endl;
//
//         std::cout << "selfToOppositeDistance = " << axisData_.selfToOppositeDistance << std::endl;
//
//         std::cout << "inAxisForwardDistances = " ;
//         for (const auto& inAxisForwardDistance : axisData_.inAxisForwardDistances){
//             std::cout << inAxisForwardDistance << ", ";
//         }
//         std::cout << std::endl;
//
//         std::cout << "inAxisBackwardDistances = " ;
//         for (const auto& inAxisBackwardDistance : axisData_.inAxisBackwardDistances){
//             std::cout << inAxisBackwardDistance << ", ";
//         }
//         std::cout << std::endl;
//
//         std::cout << "innerVolVarsDofs = ";
//         for (unsigned int i = 0; i < numPairs; ++i)
//         {
//
//             std::cout << ", pair " << i << ":";
//             for (const auto& innerVolVarDof : volVarsData_[i].innerVolVarsDofs)
//             {
//                 std::cout << innerVolVarDof << ", ";
//             }
//         }
//         std::cout << std::endl;
//
//         //check sums of interpolation factors
//         //TODO make this only happen in debug mode
//         Scalar sum = 0.;
//         std::cout << "oppoFactors ";
//         for (const auto& factor : axisData_.oppositeDofsInterpolationFactors)
//         {
//             std::cout << factor << ", ";
//             sum += factor;
//         }
//         std::cout << std::endl;
//         if (sum != 1.0)
//         {
//             DUNE_THROW(Dune::InvalidStateException, "Opposite dofs interpolation factors have to sum up to one, but they sum up to" << sum << ".");
//         }
//
//         for (const auto& data : pairData_)
//         {
//             sum = 0.;
//             std::cout << "normalPairFirstFactors ";
//             for (const auto& factor : data.normalPairInterpolationFactors.first)
//             {
//                 std::cout << factor << ", ";
//                 sum += factor;
//             }
//             std::cout << std::endl;
//             if (sum != 1.0)
//             {
//                 DUNE_THROW(Dune::InvalidStateException, "Normal pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
//             }
//         }
//
//         if(intersection_.neighbor())
//         {
//             for (const auto& data : pairData_)
//             {
//                 sum = 0.;
//                 std::cout << "normalPairsecondFactors ";
//                 for (const auto& factor : data.normalPairInterpolationFactors.second)
//                 {
//                     std::cout << factor << ", ";
//                     sum += factor;
//                 }
//                 std::cout << std::endl;
//                 if (sum != 1.0)
//                 {
//                     DUNE_THROW(Dune::InvalidStateException, "Normal pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
//                 }
//             }
//         }
//
//         for (const auto& data : pairData_)
//         {
//             for (unsigned int i = 0; i < data.parallelDofsInterpolationFactors.size(); ++i)
//             {
//                 sum = 0.;
//
//                 std::cout << "parallelFactors ";
//                 for (const auto& factor : data.parallelDofsInterpolationFactors[i])
//                 {
//                     std::cout << factor << ", ";
//                     sum += factor;
//                 }
//                 std::cout << std::endl;
//
//                 if (sum != 1.0 && data.parallelDofs[i][0] != -1)
//                 {
//                     DUNE_THROW(Dune::InvalidStateException, "Parallel pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
//                 }
//             }
//         }
//
//         for (unsigned int i = 0; i < numPairs; ++i)
//         {
//             sum = 0.;
//
//             std::cout << "innerVolVarsDofsInterpolationFactors = ";
//             for (const auto& factor : volVarsData_[i].innerVolVarsDofsInterpolationFactors)
//             {
//                 std::cout << factor << ",";
//                 sum += factor;
//             }
//             std::cout << std::endl;
//
//             if (!scalarCmp(sum, 1.0))
//             {
//                 DUNE_THROW(Dune::InvalidStateException, "vol vars inner interpolation factors have to sum up to one, but they sum up to" << sum << ".");
//             }
//         }
    }

    /*!
     * \brief Returns the local index of the face (i.e. the intersection)
     */
    int localFaceIndex() const
    {
        return intersectionMapper_.isIndexInInside(intersection_);
    }

    /*!
     * \brief Returns the local indices of the opposing faces
     */
    std::vector<int> localIndicesOpposingFace() const
    {
        const auto inIdx = intersectionMapper_.isIndexInInside(intersection_);
        return localOppositeIndices_(inIdx, this->element_);
    }

    /*!
     * \brief Returns a copy of the axis data
     */
    auto axisData() const
    {
        return axisData_;
    }

    /*!
     * \brief Returns a copy of the pair data
     */
    auto pairData() const
    {
        return pairData_;
    }

    /*!
     * \brief Returns a copy of the volume variables data
     */
    auto volVarsData() const
    {
        return volVarsData_;
    }

    /*!
     * \brief Returns the dirction index of the primary facet (0 = x, 1 = y, 2 = z)
     */
    int directionIndex() const
    {
        return Dumux::directionIndex(std::move(intersection_.centerUnitOuterNormal()));
    }

    std::array<int, dim-1> nonDirectionIndices() const
    {
        std::array<int, dim-1> retArray; //initialize just to avoid a "may be used uninitialized" warning

        int vectorPos = 0;
        for (int i = 0; i < dim; ++i)
        {
            if (vectorPos <= dim-1)
            {
                if (i != directionIndex())
                {
                    retArray[vectorPos] = i;
                    ++vectorPos;
                }
            }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Should not happen.");
            }
        }
        return retArray;
    }

    /*!
     * \brief Returns the dirction index of the facet passed as an argument (0 = x, 1 = y, 2 = z)
     */
    int directionIndex(const Intersection& intersection) const
    {
        return Dumux::directionIndex(std::move(intersection.centerUnitOuterNormal()));
    }

    //! \brief Returns the order needed by the scheme
    static int order()
    {
        return order_;
    }

    //! \brief Set the order needed by the scheme
    static void setOrder(const int order)
    {
        order_ = order;
    }

private:
    /*!
     * \brief Fills all entries of the in axis data
     */
    void fillAxisData_()
    {
        // Clear the containers before filling them
        this->axisData_.inAxisForwardDofs.clear();
        this->axisData_.inAxisBackwardDofs.clear();
        this->axisData_.inAxisForwardDistances.clear();
        this->axisData_.inAxisBackwardDistances.clear();

        // Set the self Dof
        const auto inIdx = intersectionMapper_.isIndexInInside(intersection_);
        this->axisData_.selfDof = intersectionMapper_.globalIntersectionIndex(this->element_, inIdx);

        // Set the opposite Dof
        fillOppositeDofs_();
    }

    /*!
     * \brief Fills all entries of the volume variables data
     */
    void fillVolVarsData_()
    {
        unsigned int localSubFaceIdx = 0;

        // Clear the containers before filling them
        for(auto& data : volVarsData_)
        {
            data.innerVolVarsDofs.clear();
            data.innerVolVarsDofsInterpolationFactors.clear();
            data.outerVolVarsDofs.clear();
            data.outerVolVarsDofsInterpolationFactors.clear();

            fillInnerVolVarsData_(localSubFaceIdx);

//             fillOuterVolVarsData_(localSubFaceIdx);

            ++localSubFaceIdx;
        }
    }

    /*!
     * \brief Fills all entries of the volume variables of the reference element
     */
    void fillOuterVolVarsData_(unsigned int localSubFaceIdx)
    {
        //so far for rectengular domains

        Intersection normalFace;
        Intersection oppositeFace;

        const auto& intersectionNormal = intersection_.centerUnitOuterNormal();
        auto minusIntersectionNormal = intersectionNormal;
        minusIntersectionNormal *= -1.;

        unsigned int isIdx = 0;
        for (const auto& elementIs : intersections(gridView_, element_))
        {
            const auto& normalIndices = pairData_[localSubFaceIdx].localNormalFaceIndices;
            if (std::find(normalIndices.begin(), normalIndices.end(),isIdx) != normalIndices.end() && haveCommonCorner(elementIs, intersection_))
            {
                normalFace = elementIs;
            }

            if (containerCmp(elementIs.centerUnitOuterNormal(),minusIntersectionNormal))
            {
                oppositeFace = elementIs;
            }

            ++isIdx;
        }

        if (normalFace.neighbor() && intersection_.neighbor() && oppositeFace.neighbor())
        {
            if (normalFace.inside().level() > normalFace.outside().level() && intersection_.inside().level() == intersection_.outside().level())
            {
                bool isEasierGeometry = false;
                const auto outsideGeometry = normalFace.outside().geometry();
                for (unsigned int i = 0; i < outsideGeometry.corners(); ++i)
                {
                    for (unsigned int j = 0; j < intersection_.geometry().corners(); ++j)
                    {
                        if ( containerCmp(intersection_.geometry().corner(j), outsideGeometry.corner(i)))
                        {
                            isEasierGeometry = true;
                        }
                    }
                }

                Element cornerNeighbor0;
                Element cornerNeighbor1;

                bool hasFineCornerNeighbor = false; //corner neighbor here is neighbor with which element_ touches in one corner only and with which normalFace.outside touches in one intersection_
                bool hasCornerNeighbor = true;
                unsigned int cornerNeighborCounter = 0;
                for (const auto& normalFaceNeighborIntersection : intersections(gridView_, normalFace.outside()))
                {
                    const auto& comparisonNormal = isEasierGeometry ? intersectionNormal : minusIntersectionNormal;

                    if (containerCmp(normalFaceNeighborIntersection.centerUnitOuterNormal(),comparisonNormal))
                    {
                        if (normalFaceNeighborIntersection.neighbor())
                        {
                            hasFineCornerNeighbor = (normalFaceNeighborIntersection.outside().level() > normalFace.outside().level());
                            if (cornerNeighborCounter == 0)
                            {
                                cornerNeighbor0 = normalFaceNeighborIntersection.outside();
                            }
                            else if (cornerNeighborCounter == 1)
                            {
                                cornerNeighbor1 = normalFaceNeighborIntersection.outside();
                            }
                            ++cornerNeighborCounter;
                        }
                        else
                        {
                            hasCornerNeighbor = false;
                        }
                    }
                }

                if (hasFineCornerNeighbor && hasCornerNeighbor)
                {
                    if (isEasierGeometry)
                    {
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(normalFace.outside()));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(4/9);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(cornerNeighbor0));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/9);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(cornerNeighbor1));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/9);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(element_));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/9);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(intersection_.outside()));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/9);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(oppositeFace.outside()));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/9);
                    }
                    else
                    {
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(normalFace.outside()));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(2/3);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(cornerNeighbor0));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/6);
                        volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(cornerNeighbor1));
                        volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(1/6);
                    }
                }
                else if (hasCornerNeighbor)
                {
                    volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(normalFace.outside()));
                    volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(0.75);
                    volVarsData_[localSubFaceIdx].outerVolVarsDofs.push_back(gridView_.indexSet().index(cornerNeighbor0));
                    volVarsData_[localSubFaceIdx].outerVolVarsDofsInterpolationFactors.push_back(0.25);
                }
            }
        }
        else
        {
        }
    }

    /*!
     * \brief Fills all entries of the volume variables of the reference element
     */
    void fillInnerVolVarsData_(unsigned int localSubFaceIdx)
    {
        bool linearInterpolation = getParam<bool>("Adaptivity.LinearInterpolation", true);

        if (localSubFaceIdx > 0)
        {
            volVarsData_[localSubFaceIdx].innerVolVarsDofs = volVarsData_[0].innerVolVarsDofs;
            volVarsData_[localSubFaceIdx].innerVolVarsDofsInterpolationFactors = volVarsData_[0].innerVolVarsDofsInterpolationFactors;
        }
        else{
            //TODO figure out for 3D, get a distance for elementWidthSelf and so on, they are currently areas
            if(intersection_.neighbor() && intersection_.outside().level() > element_.level())
//
//          linear:                       quadratic:
//          -------------                       ---------                 ---------              ---------
//          |     |     |                       |   |   |                 |   |   |              |   |   |
//          -------------                       ---------                 ---------              ---------
//          |     |     |                       |   |   |                 |   |   |              |   |   |
//          -------======-----------      ----------=====---------    --------=====--------     /----=====--------
//          |     *    |*|         |      |     |   *  |*|       |    | |o|   *  |*|      |     /|   *  |*|      |
//          |     o**x***     o    |      |  o  |   o*x**    o   |    -----   o*x**   o   |      o   o*x**   o   |
//          |           |          |      |     |       |        |    | |o|       |       |     /|       |       |
//          ------------------------      ------------------------    ---------------------     /-----------------
//                                        <----><------><-------->
//                                        width   width    width
//                                        far     self     close
//                or
//          -------------                       ---------                ---------              ---------
//          |     |     |                       |   |   |                |   |   |              |   |   |
//          -------------                       ---------                ---------              ---------
//          |     |     |                       |   |   |                |   |   |              |   |   |
//          -------======-----------      ----------=====--------    --------=====--------     /----=====--------
//          |     *    |*| o  |    |      |     |   *  |*|o |   |    | |o|   *  |*|o |   |     /|   *  |*|o |   |
//          |     o**x***-----------      |  o  |   o*x**--------    -----   o*x**--------      O   o*x**--------
//          |           |  o  |    |      |     |       | o |   |    | |o|       | o |   |     /|       | o |   |
//          ------------------------      -----------------------    ---------------------     /-----------------
//                                                                     <-><------><-->
//                                                                  width  width  width
//                                                                  far    self   close
//                or
//          -------------             ---------          ---------        ---------
//          |     |     |             |   |   |          |   |   |        |   |   |
//          -------------             ---------          ---------        ---------
//          |     |     |             |   |   |          |   |   |        |   |   |
//          -------====== /     ----------===== /    --------===== /     /----===== /
//          |     *    |*|/     |     |   *  |*|/    | |o|   *  |*|/     /|   *  |*|/
//          |     o**x**O /     |  o  |   o*x*O /    -----   o*x*O /      O   o*x*O /
//          |           | /     |     |       | /    | |o|       | /     /|       | /
//          ------------- /     ---------------      ------------- /     / -------- /
//
//  ***: half-control volume for the intersection
//  1/4, 3/4: interpolation factors of those cell-centered quantities in the 2D equidistant case
//  ===: intersection
//  |*|: normal face
//    o: dofs from which we interpolate
//    x: virtual dof position
//    O: In the case of a Neumann/Dirichlet boundary, there is no boundary value here. This position is not part of the dofs that we interpolate from. However, we still do factorInnter*innerValue+factorBoundary*boundaryValue while setting innerValue=boundaryValue. This is fine in the case of linear interpolation but in the case of quadratic interpolation //TODO solve this
//       In the case of an outflow boundary, there is a boundary value here.
            {
                // Let the quadratic function that is used for interpolation f(x)=a*x^2+b*x+c.
                // x=0 at the spot of interpolation,
                // distances have to carry the corrsponding signs, i.e. f(x=0)=c is the interpolated value
                // f_self = a*distanceSelf^2 + b*distanceSelf + c
                // f_close = a*distanceClose^2 + b*distanceClose + c
                // f_far = a*distanceFar^2 + b*distanceFar + c
                // solve the equation system for a,b,c
                // get c = prefactor_self(distanceSelf, distanceClose, distanceFar) * f_self +
                // prefactor_close(distanceSelf, distanceClose, distanceFar) * f_close +
                // prefactor_far(distanceSelf, distanceClose, distanceFar) * f_far

                int dofSelf;
                dofSelf = gridView_.indexSet().index(element_);
                Scalar elementWidthSelf;
                elementWidthSelf = 2. * intersection_.geometry().volume();
                Scalar distanceSelf = 0.25 * elementWidthSelf; //positive sign here is just a choice

                std::vector<int> dofsClose;
                Scalar elementWidthClose;

                bool isTwoLevelDiagonal = false; //This is not treating all cases of two diagonal levels. There are other ones which just work fine in the !isTwoLevelDiagonal without special treatment.

                //check if diagonally differing by 2 levels
                for (const auto& elementIs : intersections(gridView_, element_))
                {
                    if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(elementIs), element_))
                    {
                        if(elementIs.neighbor())
                        {
                            if(intersection_.outside().level() == elementIs.outside().level() + 2)
                            {
                                isTwoLevelDiagonal = true;
                            }
                        }
                    }
                }

                if(isTwoLevelDiagonal)
                {
                    //Initialize heights and widths for interpolations
                    std::vector<Scalar> interpFactsClose;
                    std::vector<int> dofsForDiagonal;
                    std::vector<Scalar> interpFactorsForDiagonal;
                    Scalar interpFactorCalc; //temporary variable for computing interpFactors
                    Scalar selfElementWidth;
                    Scalar normalElementHeight;
                    Scalar notOppoElementWidth;
                    auto minusNormal = intersection_.centerUnitOuterNormal();
                    minusNormal *= (-1);

                    //get selfElementWidth
                    for (const auto& elementIs : intersections(gridView_, element_))
                    {
                        if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(elementIs), element_))
                        {
                            selfElementWidth = elementIs.geometry().volume(); //width of element_
                        }

                        //obtain size of normalElementHeight (=h in image)
                        if (elementIs.neighbor())
                        {
                            for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                            {
                                if(haveCommonCorner(neighborElemIntersection, intersection_) && containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()))
                                {
                                    normalElementHeight = neighborElemIntersection.geometry().volume();
                                }
                            }
                        }
                    }

                    //obtain size for notOppoElementWidth
                    for (const auto& intersectionInsideIs : intersections(gridView_, intersection_.outside()))
                    {
                       if (haveCommonCorner(intersectionInsideIs, intersection_) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(intersectionInsideIs), element_))
                        {
                            notOppoElementWidth = intersectionInsideIs.geometry().volume();
                        }
                    }

           //         ________________________________________________________________________
           //         |                 |                 |                                   |
           //         |                 |                 |                                   |
           //         |                 |                 |                                   |
           //         |                 |                 |                                   |
           //         |                 |                 |                                   |
           //         |_________________|_________________|                                   |
           //         |                 |                 |                                   h
           //         |                 |                 |                                   h
           //         |                 |                 |                                   h
           //         |                 |                 |                                   h
           //         |                 |                 |                                   h
           //         |_________________|_____________=====wwwwwwwwwwwwwwwwww_________________h
           //         |                 |        |    =    s        =       |                 |
           //         |                 |        |    =    s        =       |                 |
           //         |                 |________|____=====s=========       |                 |
           //         |                 |        |        |                 |                 |
           //         |                 |        |        |     element_    |                 |
           //         |_________________|________|nnnnnnnn|_________________|_________________|
           //         |                 |                 |                 |                 |
           //         |                 |                 |                 |                 |
           //         |                 |                 |                 |                 |
           //         |                 |                 |                 |                 |
           //         |                 |                 |                 |                 |
           //         |_________________|_________________|_________________|_________________|

                    //s: selfElementHeight = intersection_.geometry().volume()
                    //w: selfElementWidth
                    //n: notOppoElementWidth
                    //h: normalElementHeight
                    //=: control volume for the inspected DOF which lies in s; w and s replace = at times


                    //Another height for interpolation
                    Scalar selfElementHeight = intersection_.geometry().volume(); //half of the height of elem_

                    //calculate interpFactor for selfDof
                    dofsForDiagonal.push_back(dofSelf);
                    interpFactorCalc = 0.5*(1.0 - (0.5*selfElementWidth)/(selfElementWidth + 0.5*notOppoElementWidth)) *
                                           (1.0 - (0.5*selfElementHeight)/(selfElementHeight + normalElementHeight));
                    interpFactorsForDiagonal.push_back(interpFactorCalc);

                    //calculate interpFactors for other data points
                    for (const auto& elementIs : intersections(gridView_, element_))
                    {   //choose elem above self
                        if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(elementIs), element_))
                        {
                            dofsForDiagonal.push_back(gridView_.indexSet().index(elementIs.outside()));

                            interpFactorCalc = (1.0 - (0.5*selfElementWidth)/(selfElementWidth + 0.5*notOppoElementWidth)) *
                                               (1.0 - (normalElementHeight + 0.5*selfElementHeight)/(selfElementHeight + normalElementHeight));
                            interpFactorsForDiagonal.push_back(interpFactorCalc);
                        }

                        //choose elem right of self
                        if (containerCmp(elementIs.centerUnitOuterNormal(), minusNormal)) //cannot be refined on right side, dont need to check therefore
                        {
                            dofsForDiagonal.push_back(gridView_.indexSet().index(elementIs.outside()));

                            interpFactorCalc = 0.5*(1.0 - (0.5*selfElementWidth)/(selfElementWidth + 0.5*notOppoElementWidth)) *
                                                   (1.0 - (0.5*selfElementHeight)/(selfElementHeight + normalElementHeight));
                            interpFactorsForDiagonal.push_back(interpFactorCalc);
                        }
                    }

                    //choose left of self
                    dofsForDiagonal.push_back(gridView_.indexSet().index(intersection_.outside()));

                    interpFactorCalc = (1.0 - (0.5*selfElementWidth + 0.5*notOppoElementWidth)/(selfElementWidth + 0.5*notOppoElementWidth));
                    interpFactorsForDiagonal.push_back(interpFactorCalc);

                    //output for data points and their respective interpolation factors
//                     for (unsigned int i = 0; i < dofsForDiagonal.size(); ++i)
//                     {
//                         std::cout << std::endl << "dofsForDiagonal["<<i<<"]= " << dofsForDiagonal[i];
//                         std::cout << std::endl << "interpFactorsForDiagonal["<<i<<"]= " << interpFactorsForDiagonal[i];
//                     }
//
//                     std::cout << std::endl;


                    //fill VolVars
                    for (unsigned int i = 0; i < dofsForDiagonal.size(); ++i)
                    {
                        volVarsData_[localSubFaceIdx].innerVolVarsDofs.push_back(dofsForDiagonal[i]);
                        volVarsData_[localSubFaceIdx].innerVolVarsDofsInterpolationFactors.push_back(interpFactorsForDiagonal[i]);
                    }

                    if (!linearInterpolation)
                    {
                        DUNE_THROW(Dune::InvalidStateException, "No quadratic interpolation availabel for diagonally differing by two levels.");
                    }
                }
                else
                {
                    for (const auto& elementIs : intersections(gridView_, element_))
                    {
                        if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(elementIs), element_))
                        {
                            fillCloseAndFar_(elementIs, elementWidthClose, dofsClose);
                        }
                    }
                    Scalar distanceClose = - 0.25 * elementWidthSelf - 0.5 * elementWidthClose; //negative sign here is just a choice

                    std::vector<int> dofsFar;
                    Scalar elementWidthFar;
                    Scalar distanceFar = 0.;

                    if (!linearInterpolation)
                    {
                        for (const auto& elementIs : intersections(gridView_, element_))//extra loop for far because of the break directive
                        {
                            if (!haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(elementIs), element_))
                            {
                                fillCloseAndFar_(elementIs, elementWidthFar, dofsFar);
                                break;
                            }
                        }
                        distanceFar = 0.75 * elementWidthSelf + 0.5 * elementWidthFar;
                    }

                    Scalar interpFactSelf;
                    if (linearInterpolation)
                    {
                        interpFactSelf = linearInterpolationFactor_(distanceSelf, distanceClose);
                    }
                    else
                    {
                        interpFactSelf = quadraticInterpolationFactor_(distanceSelf, distanceClose, distanceFar);
                    }
                    volVarsData_[localSubFaceIdx].innerVolVarsDofs.push_back(dofSelf);
                    volVarsData_[localSubFaceIdx].innerVolVarsDofsInterpolationFactors.push_back(interpFactSelf);

                    std::vector<Scalar> interpFactsClose;
                    for (unsigned int i = 0; i < dofsClose.size(); ++i)
                    {
                        if (linearInterpolation)
                        {
                            interpFactsClose.push_back(linearInterpolationFactor_(distanceClose, distanceSelf)/dofsClose.size());
                        }
                        else
                        {
                            interpFactsClose.push_back(quadraticInterpolationFactor_(distanceClose, distanceSelf, distanceFar)/dofsClose.size());
                        }
                    }
                    for (unsigned int i = 0; i < dofsClose.size(); ++i)
                    {
                        volVarsData_[localSubFaceIdx].innerVolVarsDofs.push_back(dofsClose[i]);
                        volVarsData_[localSubFaceIdx].innerVolVarsDofsInterpolationFactors.push_back(interpFactsClose[i]);
                    }

                    std::vector<Scalar> interpFactsFar;
                    if (!linearInterpolation)
                    {
                        for (unsigned int i = 0; i < dofsFar.size(); ++i)
                        {
                            interpFactsFar.push_back(quadraticInterpolationFactor_(distanceFar, distanceSelf, distanceClose)/dofsFar.size());
                        }
                        for (unsigned int i = 0; i < dofsFar.size(); ++i)
                        {
                            volVarsData_[localSubFaceIdx].innerVolVarsDofs.push_back(dofsFar[i]);
                            volVarsData_[localSubFaceIdx].innerVolVarsDofsInterpolationFactors.push_back(interpFactsFar[i]);
                        }
                    }
                }
            }
            else
            {
                volVarsData_[localSubFaceIdx].innerVolVarsDofs.push_back(gridView_.indexSet().index(element_));
                volVarsData_[localSubFaceIdx].innerVolVarsDofsInterpolationFactors.push_back(1.);
            }
        }
    }

    Scalar quadraticInterpolationFactor_(Scalar ownDistance, Scalar otherDistance1, Scalar otherDistance2)
    {
        return otherDistance1 * otherDistance2 / ((otherDistance1 - ownDistance) * (otherDistance2 - ownDistance));
    }

    Scalar linearInterpolationFactor_(Scalar ownDistance, Scalar otherDistance)
    {
        return otherDistance / (otherDistance - ownDistance);
    }

    void fillCloseAndFar_(const Intersection& elementIs, Scalar& elementWidth, std::vector<int>& dofs)
    {
        if (!elementIs.neighbor())
        {
            dofs.push_back(-1);
            elementWidth = 0.;
        }
        else
        {
            dofs.push_back(gridView_.indexSet().index(elementIs.outside()));

            for (const auto& outsideElemIs : intersections(gridView_, elementIs.outside()))
            {
                if (containerCmp(outsideElemIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()))
                {
                    if (!outsideElemIs.neighbor() ||
                        (elementIs.outside().level() == element_.level() && (outsideElemIs.inside().level() == outsideElemIs.outside().level()))
//          -------------
//          |  |  |     |
//          ------- out |
//          |  |  |     |
//          ---===-~~~~~|
//          |     :     |
//          |elem_: in  |
//          |     :     |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
                        ||
                        (elementIs.outside().level() > element_.level() && (outsideElemIs.inside().level() >= outsideElemIs.outside().level()))
//          -------------
//          |  |  |     |
//          ------- out |
//          |  |  |     |
//          ---===-~~---|
//          |     :in|  |
//          |elem_-------
//          |     |  |  |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
//          -------
//          |  |  |
//          ------- 3 different levels possible in this upper right corner (like elem_, like in, or even finer)
//          |  |  |
//          ---===-------
//          |     |ou|  |
//          |elem_-~~----
//          |     :in|  |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
//          -------------
//          |  |  |  |  |
//          ------------|
//          |  |  |ou|  |
//          ---===-~~---|
//          |     :in|  |
//          |elem_-------
//          |     |  |  |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
                    )
                    {
                        elementWidth = outsideElemIs.geometry().volume();
                    }
                    else if (elementIs.outside().level() == element_.level() && outsideElemIs.inside().level() < outsideElemIs.outside().level())
                    {
//          -------------
//          |  |  |  |  |
//          ----------- |
//          |  |  |ou|  |
//          ---===-~~---|
//          |     :     |
//          |elem_: in  |
//          |     :     |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
                        elementWidth = 2 * outsideElemIs.geometry().volume();
                    }
                    else if (elementIs.outside().level() == element_.level() && outsideElemIs.inside().level() > outsideElemIs.outside().level())
                    {
//                --------------
//                |            |
//                |            |
//                |            |
//          -------    out     |
//          |  |  |            |
//          -------            |
//          |  |  |            |
//          ---===-~~~~~--------
//          |     :     |
//          |elem_: in  |
//          |     :     |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
                        DUNE_THROW(Dune::InvalidStateException, "two levels direct neighbors");
                    }
                    else if (elementIs.outside().level() < element_.level())
//          -------------
//          |  |  |     |
//          ------- out |
//          |  |  |     |
//          ---===-~~~~~--------
//          |     :            |
//          |elem_:            |
//          |     :            |
//          ------|     in     |
//                |            |
//                |            |
//                |            |
//                --------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
                    {
                        DUNE_THROW(Dune::InvalidStateException, "two levels difference diagonal neighbors, should not enter here");
                    }
                    else if (elementIs.outside().level() > element_.level() && (outsideElemIs.inside().level() < outsideElemIs.outside().level()))
//          ---------------
//          |  |  | ? | ? |
//          --------------|
//          |  |  | | |   |
//          |  |  |---| ? |
//          |  |  | | |   |
//          ---===-~------|
//          |     :in |   |
//          |elem_--------|
//          |     |   |   |
//          ---------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs i:inside of outsideElemIs out:outside of outsideElemIs, ?: possibly also refined
                    {
                        // This is fine to be skipped, as the elementWidthClose will always be filled in the case of the following geometry which also will enter fillCloseAndFar_ before elementWidthClose is used.
//          -------
//          |  |  |
//          ------- 3 different levels possible in this upper right corner (like elem_, like in, or even finer)
//          |  |  |
//          ---===-------
//          |     |ou|  |
//          |elem_-~~----
//          |     :in|  |
//          -------------
//
//elem_: elment_    ===: intersection_  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
                    }
                    else
                    {
                        DUNE_THROW(Dune::InvalidStateException, "should not happen");
                    }
                }
            }
        }
    }

    /*!
     * \brief Fills all entries of the pair data
     */
    void fillPairData_()
    {
        // initialize values that could remain unitialized if the intersection lies on a boundary
        for(auto& data : pairData_)
        {
            int numParallelDofs = order_;

            // parallel Dofs
            data.parallelDofs.clear();
            data.parallelDofs.resize(numParallelDofs);

            data.parallelDofsInterpolationFactors.clear();
            data.parallelDofsInterpolationFactors.resize(numParallelDofs);

            data.virtualFirstParallelFaceDofPos = 0.;

            // parallel Distances
            data.parallelDistances.clear();
            data.parallelDistances.resize(numParallelDofs + 1, 0.0);

            // outer normals
            data.normalPair.second.resize(1);
            data.normalPair.second[0] = -1;
        }

        // get the inner normal Dof Index
        std::array<Scalar, numPairs> firstNormalDistancesContribution = {};
        std::array<Scalar, numPairs> secondNormalDistancesContribution = {};

        //first
        setNormalPairInfos_(0, firstNormalDistancesContribution);
        //second
        setNormalPairInfos_(1, secondNormalDistancesContribution);

        //might at some point include normalDistance in non-pair data
        for(unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; localSubFaceIdx++){
            auto& data = pairData_[localSubFaceIdx];
            data.normalDistance = firstNormalDistancesContribution[localSubFaceIdx] + secondNormalDistancesContribution[localSubFaceIdx];
        }

        assert(scalarCmp(firstNormalDistancesContribution[0], firstNormalDistancesContribution[1]));
        this->axisData_.selfToOppositeDistance  = 2 * firstNormalDistancesContribution[0];

        // get the parallel Dofs
        // TODO adapt to higher order
        int numPairParallelIdx = 0;

        if (dim == 2)
        {
            for (auto& pairDataElem : this->pairData_)
                pairDataElem.parallelDistances[0] = intersection_.geometry().volume(); //this is not the full parallel distance, there is also a parallelDistance[1]
        }
        else
        {
            std::cout << "staggeredgeometryhelper parallelDistances still need to figure that out for dim != 2. Also check the other locations where parallelDistances is filled" << std::endl;
        }

        for(const auto& normalIs : intersections(gridView_, element_))
        {
            //for cases where parallel dof is within the same element
            if (intersection_.neighbor() && (element_.level() < intersection_.outside().level()))
            {
                if (containerCmp(normalIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && !containerCmp(normalIs.geometry().center(), intersection_.geometry().center()))
                {
//                     |       |       |
//                     |       |       |
//                     |       |       |
//                     ---------========
//                     |  ^        ^   |
//                     |  |        |   |
//                     |normal  inter  |
//                     | Is    section_|
//                     |               |
//                     |    inside     |
//                     |               |
//                     -----------------
                    treatParallelSelfIntersection_(normalIs, numPairParallelIdx);
                }
            }

            if (intersection_.neighbor() && (element_.level() < intersection_.outside().level()) && !haveCommonCorner(normalIs, intersection_)) //normal where the parallel was already within the same element
            {
//                     |       |       |
//                     |       |       |
//                     |       |       |
//                     ---------========
//                    n|           ^   |
//                    o|           |   |
//                    r|        inter  |
//                    m|       section_|
//                    a|               |
//                    l|    inside     |
//                   Is|               |
//                     -----------------
//
//    also other nonnormal intersection that falls out in the next step

                /* skip this "normalIs" */
                continue;
            }

            if( facetIsNormal_(intersectionMapper_.isIndexInInside(normalIs), intersectionMapper_.isIndexInInside(intersection_), element_) && haveCommonCorner(normalIs, intersection_) )
            {
                if( normalIs.neighbor() )
                {
                    const auto insideLevel = normalIs.inside().level();
                    const auto outsideLevel = normalIs.outside().level();
                    if (insideLevel == outsideLevel)//can still have adaptive influences!
                    {
                        treatParallelStandardCase_(normalIs, numPairParallelIdx);
                    }
//                     -------------
//                     |     |^next|
//                     |     | next|
//                     |     |     |
//                     -------======
//                     |     |^next|
//                     |     | nor-|
//                     |     | mal |
//                     -------======
//                     |    normal^||in
//                     |           ||ter
//                     | inside    ||sec
//                     |           ||tion_
//                     |           ||
//                     -------------
                    else if (insideLevel < outsideLevel)
                    {
                        treatParallelInCoarserOut_(normalIs, numPairParallelIdx);
                    }
                    else //insideLevel > outsideLevel
                    {
                        treatParallelOutCoarserIn_(normalIs, numPairParallelIdx);
                    }
                }
                else
                {
                    treatNoParallel_(normalIs, numPairParallelIdx);
                }
            }
        }
    }

    /*!
     * \brief Returns the local opposing intersection index
     *
     * \param idx The local index of the intersection itself
     */
    std::vector<int> localOppositeIndices_(const int idx, const Element& element) const
    {
        std::vector<int> retVec;
        const auto myIs = getFacet_(idx, element);

        int localIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element))
        {
            bool isNotParallelSelf = false;
            bool isNotNormal = false;
            //TODO think about dim vs dimworld
            for (unsigned int direction = 0; direction < dim; ++direction)
            {
                //direction is normal to myIs
                if (direction == Dumux::directionIndex(std::move(myIs.centerUnitOuterNormal())))
                {
                    if(!scalarCmp(is.geometry().center()[direction], myIs.geometry().center()[direction]))
                    {
                        isNotParallelSelf = true;
                    }
                }
                //direction is parallel to myIs, in 3D this is two directions
                else
                {
                    int noCornerIndicator = 0;
                    for (unsigned int i=0; i < element.geometry().corners(); ++i){
                        if(!scalarCmp(is.geometry().center()[direction], element.geometry().corner(i)[direction]))
                        {
                            ++noCornerIndicator;
                        }
                    }
                    if (noCornerIndicator == element.geometry().corners())
                    {
                        isNotNormal = true;
                    }
                }
            }
            if (isNotParallelSelf == true && isNotNormal == true)
            {
                retVec.push_back(localIsIdx);
            }
            ++localIsIdx;
        }

        return retVec;
    }

    //returns the index of self and of parallelSelf
    std::vector<int> localSelfIndices_(const int idx, const Element& element) const
    {
        std::vector<int> retVec;
        const auto myIs = getFacet_(idx, element);

        int localIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element))
        {
            //TODO think about dim vs dimworld
            for (unsigned int direction = 0; direction < dim; ++direction)
            {
                //direction is normal
                if (direction == Dumux::directionIndex(std::move(myIs.centerUnitOuterNormal())))
                {
                    if(scalarCmp(is.geometry().center()[direction], myIs.geometry().center()[direction]))
                    {
                        retVec.push_back(localIsIdx);
                    }
                }
            }
            ++localIsIdx;
        }

        return retVec;
    }

    /*!
     * \brief Returns true if the intersection lies normal to another given intersection
     *
     * \param selfIdx The local index of the intersection itself
     * \param otherIdx The local index of the other intersection
     */
    bool facetIsNormal_(const int selfIdx, const int otherIdx, const Element& element) const
    {
        std::vector<int> localOppositeIndices = localOppositeIndices_(selfIdx, element);
        std::vector<int> localSelfIndices = localSelfIndices_(selfIdx, element);

        //TODO write finds as lambda functions
        return !(
        std::find(localSelfIndices.begin(), localSelfIndices.end(), otherIdx) != localSelfIndices.end()
        ||
        std::find(localOppositeIndices.begin(), localOppositeIndices.end(), otherIdx) != localOppositeIndices.end()
        );
    };

    Intersection getFacet_(const int localFacetIdx, const Element& element) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element)){
            if (localMyIsIdx == localFacetIdx)
            {
                return is;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in getFacet_ in staggeredgeometryhelper!" << std::endl;
        return intersection_;//just to avoid compiler warning
    };

    void setNormalPairInfos_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances)
    {
        int numPairIdx = 0;

        if (!(firstOrSecond == 1 && !intersection_.neighbor()))
        {
            Element element;
            int elementIntersectionIdx = 0;
            int normalCheckIdx;

            for (unsigned int i = 0; i < numPairs; ++i)
            {
                if (firstOrSecond == 0 /*first*/)
                {
                    pairData_[i].localNormalFaceIndices.clear();
                    pairData_[i].normalPair.first.clear();
                    pairData_[i].normalPairInterpolationFactors.first.clear();
                    //TODO is I make this an argument of the function setNormalPairInfos_ to avoid copying
                    element = element_;
                    normalCheckIdx = intersectionMapper_.isIndexInInside(intersection_);
                }
                else //firstOrSecond == 1
                {
                    pairData_[i].normalPair.second.clear();
                    pairData_[i].normalPairInterpolationFactors.second.clear();
                    //TODO is I make this an argument of the function setNormalPairInfos_ to avoid copying
                    element = intersection_.outside();
                    normalCheckIdx =  intersectionMapper_.isIndexInOutside(intersection_);
                }
            }

            int haveNonDirectionCornerCenterMatchCounter = 0;
            int notHaveNonDirectionCornerCenterMatchCounter = 0;
            std::array<unsigned int, numPairs> normalPairInitialSizes = {};
            for (unsigned int i = 0; i < numPairs; ++i) //those lines might go, if I am sure that {} is a zero initialization
            {
                normalPairInitialSizes[i] = 0;
            }

            std::array<unsigned int, numPairs> fluxCorrectionGeometryHelpingIndices = {};
            for (unsigned int i = 0; i < numPairs; ++i) //those lines might go, if I am sure that {} is a zero initialization
            {
                fluxCorrectionGeometryHelpingIndices[i] = 0;
            }

            for(const auto& elementIntersection : intersections(gridView_, element))
            {
                if(facetIsNormal_(elementIntersectionIdx, normalCheckIdx, element))
                {
                    //TODO make the following only be called when debug
                    if (numPairIdx >= numPairs || numPairIdx < 0)
                    {
                        DUNE_THROW(Dune::InvalidStateException, "numPairsIdx is not between 0 and numPairs-1, elementIntersection center = " << elementIntersection.geometry().center() << ", scvf  center = " << intersection_.geometry().center());
                    }

                    unsigned int inLevel;
                    unsigned int outLevel;

                    if (firstOrSecond == 0 /*first*/)
                    {
                        inLevel = element_.level();
                        outLevel = intersection_.neighbor()?intersection_.outside().level():inLevel;
                    }
                    else
                    {
                        inLevel = intersection_.outside().level();
                        outLevel = element_.level();
                    }

                    treatNormalPairCases_(firstOrSecond, normalDistances, element, elementIntersectionIdx, normalCheckIdx, haveNonDirectionCornerCenterMatchCounter, notHaveNonDirectionCornerCenterMatchCounter, normalPairInitialSizes, fluxCorrectionGeometryHelpingIndices, elementIntersection, inLevel, outLevel, numPairIdx);
                }
                elementIntersectionIdx++;
            }
        }
        else
        {
//             treat inIsBoundary
//            /----------
//            /||       |
//            /|| out=  |
//            /||element|
//            /||       |
//            /----------
//
//            /:boundary
//            ||:intersection

            // fill the normal pair entries
            for(int pairIdx = 0; pairIdx < numPairs; ++pairIdx)
            {
                assert(pairData_[pairIdx].normalPair.second[0] == -1); //pairData_[pairIdx].normalPair.second[0] is initialized with -1 and cleared in the if condition above

                normalDistances[numPairIdx] = 0.;
                numPairIdx++;
            }
        }
    }

    void treatNormalPairCases_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances, const Element& element, const int& normalIntersectionIdx, const int& normalCheckIdx, int& haveNonDirectionCornerCenterMatchCounter, int& notHaveNonDirectionCornerCenterMatchCounter, std::array<unsigned int, numPairs>& normalPairInitialSizes, std::array<unsigned int, numPairs>& fluxCorrectionGeometryHelpingIndices, const Intersection& normalIntersection, unsigned int inLevel, unsigned int outLevel, int& numPairIdx)
    {
        if ((!intersection_.neighbor()) || (inLevel >= outLevel) || (inLevel < outLevel && !haveNonDirectionCornerCenterMatch_(normalIntersection))){
            //set local information for all cases
            if (firstOrSecond == 0 /*first*/)
            {
                setNormalPairLocalInfo_(normalIntersectionIdx, element, numPairIdx);
                if (haveNonDirectionCenterCornerMatch_(normalIntersection))
                {
                    this->pairData_[numPairIdx].localNormalFluxCorrectionIndex = normalIntersectionIdx;
                }
            }

            //set global information case-dependent
            if (intersection_.neighbor() && inLevel < outLevel && (!normalIntersection.neighbor() || (normalIntersection.neighbor() && (element.level() >= normalIntersection.outside().level()))))
            {
//             |      |
//             ********--------
//             |      |   |   |
//             |  in  |--------
//             |     ||out|   |
//             ----------------
//
//             ||: intersection_
//             **: normalIntersection

                setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .5);
                normalDistances[numPairIdx] = normalIntersection.geometry().volume() * 0.5;
                numPairIdx++;
            }
            else if ((!intersection_.neighbor() || !(inLevel < outLevel)) && (!normalIntersection.neighbor() || (normalIntersection.neighbor() && (element.level() >= normalIntersection.outside().level()))))
            {
//             -----****--------
//             |   |in||       |
//             |----****  out  |
//             |   |   |       |
//             -----------------
//
//             ||: intersection_
//             **: normalIntersection

//             |       |
//             *********--------
//             |      ||       |
//             |  in  ||  out  |
//             |      ||       |
//             -----------------
//
//             ||: intersection_
//             **: normalIntersection

//             |        |
//             **********/
//             |       ||/
//             |  in=  ||/
//             |element||/
//             ----------/
//
//             /: boundary
//             ||: intersection_
//             **: normalIntersection

                setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, 1.);
                normalDistances[numPairIdx] = normalIntersection.geometry().volume() * 0.5;
                numPairIdx++;
            }
            else if (normalIntersection.neighbor() && (element.level() < normalIntersection.outside().level()))
            {
//                 | | |
//             -----*-*---------
//             |   |in||       |
//             |----****  out  |
//             |   |   |       |
//             -----------------
//
//             ||: intersection_
//             **: normalIntersection

//             |   |   |
//             **** ****--------
//             |       |   |   |
//             |  in   |--------
//             |      ||out|   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

//             |   |   |
//             **** ****--------
//             |      ||       |
//             |  in  ||  out  |
//             |      ||       |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

//             |   |    |
//             **** *****/
//             |       ||/
//             |  in=  ||/
//             |element||/
//             ----------/
//
//             /: boundary
//             ||: intersection_
//             **: normal intersections
                treatInCoarserNormalOut_(firstOrSecond, normalDistances, element, normalIntersectionIdx, normalCheckIdx, normalPairInitialSizes, fluxCorrectionGeometryHelpingIndices, normalIntersection, inLevel, outLevel, numPairIdx, notHaveNonDirectionCornerCenterMatchCounter);
            }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Should not occur.");
            }
        }
        else
        {
//             |      |
//             ********--------
//             |     ||out|   |
//             |  in  |--------
//             |      |   |   |
//             ----------------
//
//             ||: intersection_
//             **: normalIntersection
//
//             or
//
//             |   |   |
//             **** ****--------
//             |      ||out|   |
//             |  in   |--------
//             |       |   |   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

            treatNormalsWithCornerCenterMatchForInCoarserOut_(firstOrSecond, normalDistances, element, normalIntersectionIdx, normalPairInitialSizes, normalIntersection, numPairIdx, haveNonDirectionCornerCenterMatchCounter);
        }
    }

    void treatNormalsWithCornerCenterMatchForInCoarserOut_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances, const Element& element, const int& normalIntersectionIdx, std::array<unsigned int, numPairs>& normalPairInitialSizes, const Intersection& normalIntersection, int& numPairIdx, int& haveNonDirectionCornerCenterMatchCounter){
        //TODO adapt to 3D
        int otherPairIdx = (numPairIdx == 0) ? 1 : 0;

        //set local information
        if (firstOrSecond == 0 /*first*/)
        {
            setNormalPairLocalInfo_(normalIntersectionIdx, element, numPairIdx);
            setNormalPairLocalInfo_(normalIntersectionIdx, element, otherPairIdx);

            if (haveNonDirectionCenterCornerMatch_(normalIntersection))
            {
                this->pairData_[numPairIdx].localNormalFluxCorrectionIndex = normalIntersectionIdx;
            }
        }

        //set global information
        if (haveNonDirectionCornerCenterMatchCounter == 0)
        {
            for (unsigned int i = 0; i < numPairs; ++i)
            {
                if (firstOrSecond == 0)
                    normalPairInitialSizes[i] = pairData_[i].normalPair.first.size();
                else
                    normalPairInitialSizes[i] = pairData_[i].normalPair.second.size();
            }
            haveNonDirectionCornerCenterMatchCounter++;
        }

        if (!normalIntersection.neighbor() || (element.level() >= normalIntersection.outside().level()))
        {
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, 1.);
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, otherPairIdx, .5);

            normalDistances[numPairIdx] = normalIntersection.geometry().volume() * 0.5;
            numPairIdx++;
        }
        else
        {
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .5);
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, otherPairIdx, .25);

            normalDistances[numPairIdx] = normalIntersection.geometry().volume();

            //decide if numPairIdx needs to be incremented or not and do so if necessary
            unsigned int pairMemberSize;

            if (firstOrSecond == 0 /*first*/)
                pairMemberSize = pairData_[numPairIdx].normalPair.first.size();
            else
                pairMemberSize = pairData_[numPairIdx].normalPair.second.size();

            if (pairMemberSize - normalPairInitialSizes[numPairIdx] == 2)
            {
                numPairIdx++;
            }
        }
    }

    void treatInCoarserNormalOut_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances, const Element& element, const int& normalIntersectionIdx, const int& normalCheckIdx, std::array<unsigned int, numPairs>& normalPairInitialSizes, std::array<unsigned int, numPairs>& fluxCorrectionGeometryHelpingIndices, const Intersection& normalIntersection, unsigned int inLevel, unsigned int outLevel, int& numPairIdx, int& notHaveNonDirectionCornerCenterMatchCounter)
    {
        if ((!intersection_.neighbor()) || (inLevel >= outLevel))
        {
//             !intersection_.neighbor()
//             |   |    |
//             **** *****/
//             |       ||/
//             |  in=  ||/
//             |element||/
//             ----------/
//
//             /: boundary
//             ||: intersection_
//             **: normal intersections

//             inLevel > outLevel
//                 | | |
//             -----*-*---------
//             |   |in||       |
//             |--------  out  |
//             |   |   |       |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections
//
//             case of two level distance over a diagonal should already work here

//             inLevel = outLevel
//             |   |   |
//             **** ****--------
//             |      ||       |
//             |  in  ||  out  |
//             |      ||       |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

            if (isFluxCorrectionGeometry(inLevel, outLevel, firstOrSecond, normalCheckIdx, numPairIdx, element))
            {
                if (haveCommonCorner(normalIntersection, intersection_))
                {
                    setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, 1.);

                    normalDistances[numPairIdx] = 0.5*normalIntersection.geometry().volume();
                }

                if (fluxCorrectionGeometryHelpingIndices[numPairIdx] == 1)
                {
                    numPairIdx++;
                }
                else
                {
                    fluxCorrectionGeometryHelpingIndices[numPairIdx]++;
                }
            }
            else
            {
                setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .5);

                normalDistances[numPairIdx] = normalIntersection.geometry().volume();

                unsigned int pairMemberSize;

                if (firstOrSecond == 0 /*first*/)
                    pairMemberSize = pairData_[numPairIdx].normalPair.first.size();
                else
                    pairMemberSize = pairData_[numPairIdx].normalPair.second.size();

                if (pairMemberSize == 2)
                {
                    numPairIdx++;
                }
            }
        }
        else
        {
//             |   |   |
//             **** ****--------
//             |       |   |   |
//             |  in   |--------
//             |      ||out|   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections
            if (notHaveNonDirectionCornerCenterMatchCounter == 0)
            {
                for (unsigned int i = 0; i < numPairs; ++i)
                {
                    if (firstOrSecond == 0 /*first*/)
                        normalPairInitialSizes[i] = pairData_[i].normalPair.first.size();
                    else
                        normalPairInitialSizes[i] = pairData_[i].normalPair.second.size();
                }
                notHaveNonDirectionCornerCenterMatchCounter++;
            }

            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .25);

            normalDistances[numPairIdx] = normalIntersection.geometry().volume();

            unsigned int pairMemberSize;

            if (firstOrSecond == 0 /*first*/)
                pairMemberSize = pairData_[numPairIdx].normalPair.first.size();
            else
                pairMemberSize = pairData_[numPairIdx].normalPair.second.size();

            if (pairMemberSize - normalPairInitialSizes[numPairIdx] == 2)
            {
                numPairIdx++;
            }
        }
    }

    bool isFluxCorrectionGeometry(bool inLevel, bool outLevel, bool firstOrSecond, const int& normalCheckIdx, bool numPairIdx, const Element& element)
    {
        bool useConservation = getParam<bool>("Adaptivity.Conservation");

//       for the cases of no neighbor or inLevel > outLevel appearing here, the flux is corrected in fluxvariables.hh. It needs not be corrected here in the staggeredgeometryhelper. isFluxCorrectionGeometry should return false. For this I assumed that at an intersection levels can differ by 1 maximally
        if (useConservation && inLevel == outLevel && firstOrSecond == 1 /*second*/)//this is basically a flux correction, we only fill //fill booleans
        {
            unsigned int firstNormalOutLevel = inLevel; //just an initialization, should always be overwritten in cases reaching the return inLevel == firstNormalOutLevel
            int variedNormalIsIdx = 0;
            int variedPairIdx = 0;
            Intersection intersectionTouchingNormalIntersectionOfCorrectNumPair; //normalIntersection which touches the intersection

            for(const auto& variedNormalIs : intersections(gridView_, element))
            {
                if(facetIsNormal_(variedNormalIsIdx, normalCheckIdx, element))
                {
//                     flux correction geometry
//
//                     ----------------------------------
//                     |       |       |                |
//                     |       |       |                |
//                     |       |       |                |
//                     ------------**************       |
//                     |       |   *   |        *       |
//                     |       |   *   |        *       |
//                     |       |   *   |        *       |
//                     ---x1--x3**x2***?????????x4-------
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     |      in       ||     out       |
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     --------******************--------
//
//           ||: the scvf
//           ***: the control volume
//           ???: line along which the gradient normalDeltaV is to be thought about carefully, As the fine side (in the picture the upper half) along this line does a gradient between x2 and x4 (see fine control volume) the coarse side (in the picture the lower half) has to adapt its behavior accordingly. Hence not an interpolated value beween x1 and x2 but only a value at x2 serves as the normal second for the coarse control volume around the scvf

                    if(haveCommonCorner(variedNormalIs, intersection_))
                    {
                        if (numPairIdx == variedPairIdx)
                        {
                            intersectionTouchingNormalIntersectionOfCorrectNumPair = variedNormalIs;
                        }
                        variedPairIdx++; //the if-case is not complicated with respect to pair
                    }
                }
                variedNormalIsIdx++;
            }

            if (!intersectionTouchingNormalIntersectionOfCorrectNumPair.neighbor())
            {
                return false;
            }
            else
            {
                for(const auto&  diagonalOutsideIs : intersections(gridView_, intersectionTouchingNormalIntersectionOfCorrectNumPair.outside()))
                {
                    auto minusIntersectionNormal = intersection_.centerUnitOuterNormal(); //minus because only relevant for firstOrSecond = second
                    minusIntersectionNormal *= -1.;

                    if (containerCmp(diagonalOutsideIs.centerUnitOuterNormal(), minusIntersectionNormal))
                    {
                        if (!diagonalOutsideIs.neighbor())
                        {
                            return false;
                            //TODO think through for more complicated geometries with edges
                        }
                        else
                        {
                            firstNormalOutLevel = diagonalOutsideIs.outside().level();
                        }
                    }
                }
            }

            return inLevel == firstNormalOutLevel;
        }
        else
        {
            return false;
        }
    }

    //! Sets the information about the normal faces (within the element)
    void setNormalPairDofInfo_(bool firstOrSecond, const int isIdx, const Element& element, int i, Scalar interpolationFactor)
    {
        auto val = intersectionMapper_.globalIntersectionIndex(element, isIdx);

        // store the normal dofIdx
        if (firstOrSecond == 0 /*first*/)
        {
            auto& dofIdx = pairData_[i].normalPair.first;
            auto& interpFact = pairData_[i].normalPairInterpolationFactors.first;

            if (std::find(dofIdx.begin(), dofIdx.end(), val) == dofIdx.end())
            {
                dofIdx.push_back(val);
                interpFact.push_back(interpolationFactor);
            }
        }
        else /*(firstOrSecond == 1 second)*/
        {
            auto& dofIdx = pairData_[i].normalPair.second;
            auto& interpFact = pairData_[i].normalPairInterpolationFactors.second;

            if (std::find(dofIdx.begin(), dofIdx.end(), val) == dofIdx.end())
            {
                dofIdx.push_back(val);
                interpFact.push_back(interpolationFactor);
            }
        }
    }

    void setNormalPairLocalInfo_(const int isIdx, const Element& element, const int numPairsIdx)
    {
        // store the local normal facet index
        auto& localIndices = this->pairData_[numPairsIdx].localNormalFaceIndices;

        if (std::find(localIndices.begin(), localIndices.end(), isIdx) == localIndices.end())
            localIndices.push_back(isIdx);
    }

    void fillOppositeDofs_(){
        const auto localOppoIndices = localOppositeIndices_(intersectionMapper_.isIndexInInside(intersection_), this->element_);

        if (!intersection_.neighbor() || (element_.level() >= intersection_.outside().level()))
        {
//             !intersection_.neighbor()
//           ------------/      ------------/
//             |       ||/        |       ||/
//             |  in   ||/      --|  in   ||/
//             |       ||/        |       ||/
//           ------------/      ------------/
//
//             /: boundary
//             ||: intersection_

//             inLevel > outLevel
//             -----------------        -----------------
//             |   |in||       |        |-|-|in||       |
//             |--------  out  |        |--------  out  |
//             |   |   |       |        |   |   |       |
//             -----------------        -----------------
//
//             ||: intersection_

//             inLevel = outLevel
//           -------------------      -------------------
//             |      ||       |        |      ||       |
//             |  in  ||  out  |      --|  in  ||  out  |
//             |      ||       |        |      ||       |
//           -------------------      -------------------
//
//             ||: intersection_

            treatOppoInNotCoarserOut_(localOppoIndices);
        }
        else
        {
            if (localOppoIndices.size() == 1)
            {
//            -->*
//             -----------------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             -->*: a velocity value from above is considered for the interpolation

                treatOppoInCoarserOutOneLocalOppo_(localOppoIndices);
            }
            else if (localOppoIndices.size() == 2)
            {
//             -----------------
//            -->     ||out|   |
//             |  in   |--------
//            -->      |   |   |
//             -----------------
//             ||: intersection_

                treatOppoInCoarserOutTwoLocalOppo_(localOppoIndices);
            }
            else
            {
                std::cout << "localOppositeIndices should be one or two" << std::endl;
            }
        }
    }

    void treatOppoInCoarserOutTwoLocalOppo_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = this->axisData_.oppositeDofs;
        auto& oppoDofsInterp = this->axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();

        for (const auto& localOppoIndex : localOppoIndices)
        {
            const auto& oppoFacet = getFacet_(localOppoIndex, element_);

            //TODO adapt to 3d
            unsigned int nonDirectionIdx = (directionIndex() == 0) ? 1 : 0;

            if (scalarCmp(oppoFacet.geometry().center()[nonDirectionIdx], intersection_.geometry().center()[nonDirectionIdx]))
            {
                oppoDofs.push_back(intersectionMapper_.globalIntersectionIndex(this->element_, localOppoIndex));
                oppoDofsInterp.push_back(1.);
            }
        }
    }

    void treatOppoInNotCoarserOut_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = this->axisData_.oppositeDofs;
        auto& oppoDofsInterp = this->axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.resize(localOppoIndices.size());
        for (unsigned int i = 0; i < localOppoIndices.size(); ++i)
        {
            oppoDofs[i] = intersectionMapper_.globalIntersectionIndex(this->element_, localOppoIndices[i]);
        }

        if (oppoDofs.size() == 1)
        {
            oppoDofsInterp.resize(oppoDofs.size());
            oppoDofsInterp[0] = 1.0;
        }
        else if (oppoDofs.size() == 2)
        {
            oppoDofsInterp.resize(oppoDofs.size());
            oppoDofsInterp[0] = 0.5;
            oppoDofsInterp[1] = 0.5;
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "Expected oppoDofs.size to either be 1 or 2 but is not the case.");
        }
    }

    void treatOppoInCoarserOutOneLocalOppo_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = this->axisData_.oppositeDofs;
        auto& oppoDofsInterp = this->axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();

        oppoDofs.push_back(intersectionMapper_.globalIntersectionIndex(this->element_, localOppoIndices[0]));

        for (const auto& elementIs : intersections(gridView_, element_))
        {
            if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_.isIndexInInside(intersection_), intersectionMapper_.isIndexInInside(elementIs), element_))
            {
                if (elementIs.neighbor())
                {
                    auto minusNormal = intersection_.centerUnitOuterNormal();
                    minusNormal *= (-1);

                    if (elementIs.outside().level() == element_.level())
                    {
//             above here there can be a coarse cell with one velocity on the left, a coarse cell with two velocities on the left or four fine cells
//             in any of the cases, only the lower left velocity is considered
//             #       |
//             *********--------
//             |      ||out|   |
//             +      ||   |   |
//             |      ||   |   |
//            --> in   |--------
//             |       |   |   |
//             |       |   |   |
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//             #: neighborElemIntersection
//             +: interpolate a velocity here

                        for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                        {
                            if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal) && haveCommonNonDirectionalCornerCoordinate_(neighborElemIntersection, intersection_))
                            {
                                oppoDofs.push_back(intersectionMapper_.globalIntersectionIndex(elementIs.outside(), intersectionMapper_.isIndexInInside(neighborElemIntersection)));

                                Scalar distanceSelf = 0.5 * intersection_.geometry().volume(); //positive sign here is just a choice;//this is one quater of the opposite intersection, because the opposite intersection is twice the size of the self intersection in 2D
                                Scalar distanceClose = - 0.5 * intersection_.geometry().volume() - 0.5 * neighborElemIntersection.geometry().volume(); //negative sign here is just a choice

                                Scalar interpFactSelf = linearInterpolationFactor_(distanceSelf, distanceClose);
                                Scalar interpFactClose = linearInterpolationFactor_(distanceClose, distanceSelf);

                                oppoDofsInterp.push_back(interpFactSelf);
                                oppoDofsInterp.push_back(interpFactClose);
                            }
                        }
                    }
                    else if (elementIs.outside().level() > element_.level())
                    {
//             |   |   |
//             ****-****--------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: element intersections
                        if (!haveCommonCorner(elementIs, intersection_) )
                        {
//             #   |   |
//             ****-------------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//              #: neighbor element intersection
                            for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                            {
                                if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                {
                                    oppoDofs.push_back(intersectionMapper_.globalIntersectionIndex(elementIs.outside(), intersectionMapper_.isIndexInInside(neighborElemIntersection)));

                                    Scalar distanceSelf = 0.5 * intersection_.geometry().volume(); //positive sign here is just a choice;
                                    Scalar distanceClose = - 0.5 * intersection_.geometry().volume() - 0.5 * neighborElemIntersection.geometry().volume(); //negative sign here is just a choice

                                    Scalar interpFactSelf = linearInterpolationFactor_(distanceSelf, distanceClose);
                                    Scalar interpFactClose = linearInterpolationFactor_(distanceClose, distanceSelf);

                                    oppoDofsInterp.push_back(interpFactSelf);
                                    oppoDofsInterp.push_back(interpFactClose);
                                }
                            }
                        }
                    }
                    else
                    {
                        //          ________________________________________________________________________
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |_________________|_________________|                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n    element.outside                h
                        //         |                 |                 n                                   h
                        //         |_________________|_________________n==================_________________h
                        //         |                 |        |inter-  I                 |                 |
                        //         |                 |        |section.I                 |                 |
                        //         |                 |        |outside I                 |                 |
                        //         |                 |‾‾‾‾‾‾‾‾|‾‾‾‾‾‾‾‾|     element_    |                 |
                        //         |                 |        |        |                 |                 |
                        //         |_________________|________|________|_________________|_________________|
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |_________________|_________________|_________________|_________________|

                        //I: intersection_.geometry().volume()
                        //n: neighborElemIntersection.geometry().volume()
                        //=: elementIs

                        if(intersection_.neighbor())
                        {
                            //check if difference of elementIs.outside().level() and intersection_.outside().level() is >= 2
                            if(element_.level() < intersection_.outside().level())
                            {
                                //placerholder value for interpFactorSelf
                                oppoDofsInterp.push_back(0.0);

                                Scalar intersectionNormalCounter = 0.0; //count the number of oppoDofs/intersections with the same normal vector as intersection_
                                Scalar minusNormalCounter = 0.0;        //count intersections with opposite normal vector as intersection_
                                auto intersectionNormal = intersection_.centerUnitOuterNormal();
                                for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                                    { //in top neighboring elem we need dofs on all parallel intersections (parallel to intersection_) not just the one on the opposite
                                    //allow both directions for parallel normal vector of intersections (minus (opposite) and plus (in direction) of intersection_ normal
                                        if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal)
                                            || containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                        {
                                            if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                            {
                                                minusNormalCounter += 1.0; //count intersections with opposite normal vector as intersection_
                                            }

                                            if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                            {
                                                intersectionNormalCounter += 1.0; //count the number of oppoDofs/intersections with the same normal vector as intersection_
                                            }
                                        }
                                    }

                                    Scalar lengthForInterpFactorSelf = 0.0;

                                    for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                                    {
                                        Scalar interpFactor = 0.0;
                                        if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal)
                                            || containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                        {
                                            //oppoDofSelf was pushed back in the beginning of this whole function, need to assign a interpFactor to it
                                            if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                            {
                                                oppoDofs.push_back(intersectionMapper_.globalIntersectionIndex(elementIs.outside(), intersectionMapper_.isIndexInInside(neighborElemIntersection)));

                                                if(intersectionNormalCounter == 1) //should never occur, not permitted to levels difference for non-diagonals
                                                {
                                                    interpFactor = 0.5*(1.0 - (0.5*intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume()));
                                                }
                                                else if(intersectionNormalCounter == 2) //two intersections on the left side of coarse cell
                                                {
                                                    lengthForInterpFactorSelf = neighborElemIntersection.geometry().volume();

                                                    //interpFactor 0.5 from presequent interpolation already integrated into factor 0.25
                                                    interpFactor = 0.25*(1.0 - (0.5*intersection_.geometry().volume() + neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + neighborElemIntersection.geometry().volume()));
                                                }

                                                oppoDofsInterp.push_back(interpFactor);
                                            }
                                            else if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                            {
                                                oppoDofs.push_back(intersectionMapper_.globalIntersectionIndex(elementIs.outside(), intersectionMapper_.isIndexInInside(neighborElemIntersection)));

                                                if(minusNormalCounter == 1) //one intersection on the right side of coarse cell
                                                {
                                                    interpFactor = 0.5*(1.0 - (0.5*intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume()));
                                                }
                                                else if(minusNormalCounter == 2) //two intersections on the right side of coarse cell
                                                {
                                                    //interpFactor 0.5 from presequent interpolation already integrated into factor 0.25
                                                    interpFactor = 0.25*(1.0 - (0.5*intersection_.geometry().volume() + neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + neighborElemIntersection.geometry().volume()));
                                                }

                                                oppoDofsInterp.push_back(interpFactor);
                                            }
                                        }
                                    }

                                    Scalar interpFactorSelf = 0.0;
                                    interpFactorSelf = 1.0 - (0.5*intersection_.geometry().volume())/(intersection_.geometry().volume() + lengthForInterpFactorSelf);

                                    //insert right value for placeholder at [0] for self
                                    oppoDofsInterp[0] = interpFactorSelf;
                            }
                        }
                    }
                }
                else
                {
//             /////////
//             *********--------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//             //: boundary

                    //get Dirichlet boundary value and weight it by 0.5
                    oppoDofs.push_back(-1);
                    oppoDofsInterp.push_back(0.5);
                    oppoDofsInterp.push_back(0.5);
                }
            }
        }
    }

    void treatParallelSelfIntersection_(const Intersection& parallelSelfIntersection, int& numPairParallelIdx)
    {
//      Pictures here are drawn assuming that there are not fine neighbors other than at the side of the intersection. If there are additional fine neighbors this does not change anything.
//      || or ==: self intersection_
//      ***: normal intersection
//      +++: parallel self intersection
//       []: pair indices belonging to the normal intersections
// plain number: local indices

//      Four geometries with no special treatment:
//          4 [1]                       4 [1]                        4                           3      4
//      **************              **************             --------------                 =======+++++++
//    1 ||           |              |           || 2           *            |                 *            |
//      ||   in      | 2          0 |    in     ||         0[0]*    in      | 1[1]        0[0]*    in      | 1[1]
//      +            |              |            +             *            |                 *            |
//    0 +            |              |            + 1           *            |                 *            |
//      --------------              --------------             =======+++++++                 --------------
//          3 [0]                       3 [0]                     2      3                           2

        //parallel within the same element
        const auto elem = parallelSelfIntersection.inside();
        const auto idx = intersectionMapper_.isIndexInInside(parallelSelfIntersection);
        const auto globIdx = intersectionMapper_.globalIntersectionIndex(elem, idx);
        const auto& paraCenter = parallelSelfIntersection.geometry().center();
        const auto& isCenter = intersection_.geometry().center();

        Scalar eps = 1e-10;
        std::size_t previousIdx = numPairParallelIdx;

        if (paraCenter[1] > isCenter[1] + eps)
        {
//      We have to consider the pair indices because the loop for normal first, first reaches [0] then [1] and parallel should be numbered in the same way as the normal pairs.

//          4 [1]                      4 [1]
//      --------------             --------------
//    1 +            |             |            +
//      +     in     | 2         0 |    in      + 2
//      ||           |             |           ||
//    0 ||           |             |           || 1
//      **************             **************
//          3 [0]                       3 [0]
            assert (previousIdx == 0); //we arrive at the parallelSelfIs, before we arrive at the normal intersection
            numPairParallelIdx = 1;
            //TODO think through for 3D
        }

        if (paraCenter[0] < isCenter[0] - eps)
        {
//            4                       3      4
//      --------------             +++++++=======
//      |            *             |            *
//  0[0]|    in      * 1[1]    0[0]|     in     * 1[1]
//      |            *             |            *
//      |            *             |            *
//      +++++++=======             --------------
//         2     3                        2
//
//      In the above-visualized two cases, one first arrives at the normalIs, then I have to pack the values which I mistakenly wrote into pair 0 into pair 1
            assert (previousIdx == 1);
            numPairParallelIdx = 0;
            //TODO think through for 3D
            this->pairData_[1].parallelDofs[0/*order*/] = this->pairData_[0].parallelDofs[0/*order*/];
            this->pairData_[1].parallelDofsInterpolationFactors[0/*order*/] = this->pairData_[0].parallelDofsInterpolationFactors[0/*order*/];
            this->pairData_[1].parallelDistances[1] = this->pairData_[0].parallelDistances[1];

            this->pairData_[0].parallelDofs[0].clear();
            this->pairData_[0].parallelDofsInterpolationFactors[0].clear();
        }

        this->pairData_[numPairParallelIdx].parallelDofs[0].push_back(globIdx);
        this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0].push_back(1);
        this->pairData_[numPairParallelIdx].parallelDistances[1] = parallelSelfIntersection.geometry().volume();

        if (paraCenter[1] > isCenter[1] + eps)
        {
            numPairParallelIdx = previousIdx;
            //TODO think through for 3D
        }
        else
        {
            numPairParallelIdx++;
        }
    }

    void treatParallelStandardCase_(const Intersection& normalIs, int& numPairParallelIdx)
    {
//      TODO adapt for higher order

//         coarse                fine upper right neighbor(s)
//      --------------       --------------
//      |          |         |          |    not taking value here
//      |         -->        |          |--
//      |          |         |          -->  taking value only here (linear interpolation between two values)
//      ************--       ************--
//      |         ||         |         ||
//      |         ||         |         ||
//      |         ||         |         ||
//      ------------         ------------
//      |          |         |          |
//
//     **: normalIs

        std::stack<Element> parallelElementStack;
        parallelElementStack.push(element_);
        bool keepStacking =  (parallelElementStack.size() < order_+1);
        while(keepStacking)
        {
            auto e = parallelElementStack.top();
            for(const auto& nextNormalIs : intersections(gridView_, e))
            {
                if( containerCmp(nextNormalIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()) )
                {
                    if( nextNormalIs.neighbor() )
                    {
                        parallelElementStack.push(nextNormalIs.outside());
                        keepStacking = (parallelElementStack.size() < order_+1);
                    }
                    else
                    {
                        keepStacking = false;
                    }
                }
            }
        }
        while(!parallelElementStack.empty())
        {
            if(parallelElementStack.size() > 1)
            {
                for(const auto& is : intersections(gridView_, parallelElementStack.top()))
                {
                    if (containerCmp(is.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(is, intersection_))
                    {
                        this->pairData_[numPairParallelIdx].parallelDofs[parallelElementStack.size()-2].push_back(intersectionMapper_.globalIntersectionIndex(parallelElementStack.top(), intersectionMapper_.isIndexInInside(is)));
                        this->pairData_[numPairParallelIdx].parallelDistances[parallelElementStack.size()-1] = is.geometry().volume(); //parallelDistances[0] is filled in the superordinate function
                    }
                }
            }
            parallelElementStack.pop();
        }

        fillParallelInterpolationFactors_(numPairParallelIdx);

        numPairParallelIdx++;
    }

    void treatParallelInCoarserOut_(const Intersection& normalIs, int& numPairParallelIdx)
    {
//                     -------------
//                     |     |^next|
//                     |     | next|
//                     |     |     |
//                     -------======
//                     |     |^next|
//                     |     | nor-|
//                     |     | mal |
//                     -------======
//                     |    normal^||in
//                     |           ||ter
//                     | inside    ||sec
//                     |           ||tion_
//                     |           ||
//                     -------------
        bool wantToContinue = false;
        for (unsigned int i = 0; i < normalIs.geometry().corners(); ++i)
        {
            if (scalarCmp(normalIs.geometry().corner(i)[directionIndex()], intersection_.geometry().center()[directionIndex()]))
            {
                wantToContinue = true;
            }
        }
        if (wantToContinue)
        {
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
//                 if( containerCmp(nextElementIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()) )
//                 {
//                     const auto& nextNormalIs = nextElementIs;
//
//                     if (nextNormalIs.neighbor())
//                     {
//                         for (const auto& nextNextElementIs : intersections(gridView_, nextNormalIs.outside()))
//                         {
//                             if (containerCmp(nextNextElementIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) )
//                             {
//                                 const auto& nextNextParallelIs = nextNextElementIs;
//                                 this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_.globalIntersectionIndex(nextNormalIs.outside(), intersectionMapper_.isIndexInInside(nextNextParallelIs)));
//                                 this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0].push_back(.5);
//                                 this->pairData_[numPairParallelIdx].parallelDistances[1] = nextNextParallelIs.geometry().volume(); //TODO think if +=
//                             }
//                         }
//                     }
//                     else
//                     {
//                         //TODO write what should happen here
//                     }
//                 }
                /*else */if ( containerCmp(nextElementIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) )
                {
                    bool isTwoLevelDiagonal = nextElementIs.neighbor() && nextElementIs.inside().level() < nextElementIs.outside().level();

                    if (!isTwoLevelDiagonal || (isTwoLevelDiagonal && haveCommonCorner(nextElementIs, intersection_)))
                    {
                        const auto& nextParallelIs = nextElementIs;
                        this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_.globalIntersectionIndex(normalIs.outside(), intersectionMapper_.isIndexInInside(nextParallelIs)));
                        this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0].push_back(/*.5*/1.);
                        this->pairData_[numPairParallelIdx].parallelDistances[1] += nextParallelIs.geometry().volume();
                    }
                }
            }
            numPairParallelIdx++;
        }
    }

    void treatParallelOutCoarserIn_(const Intersection& normalIs, int& numPairParallelIdx)
    {
        bool isEasierGeometry = false;
        const auto outsideGeometry = normalIs.outside().geometry();
        for (unsigned int i = 0; i < outsideGeometry.corners(); ++i)
        {
            if (dim == 2 && i == 4)
            {
                DUNE_THROW(Dune::InvalidStateException, "Expected 4 corners of the rectangle only.");
            }

            for (unsigned int j = 0; j < intersection_.geometry().corners(); ++j)
            {
                if ( containerCmp(intersection_.geometry().corner(j), outsideGeometry.corner(i)))
                {
                    isEasierGeometry = true;
                }
            }
        }

//                     -------------
//            this left|           |
//  neighbor determines|           |
//if true one or twice,|           |
// parallel dofs only  |           |
// from the left       |           |
//                     ======-------
//                    ||     |     |
//                    ||     |     |
//                    ||     |     |
//                     -------------
// ||:myintersection, =: normalIntersection

        if (isEasierGeometry )
        {
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
                if ( containerCmp(nextElementIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(nextElementIs, intersection_) )
                {
                    const auto& nextParallelIs = nextElementIs;
                    this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_.globalIntersectionIndex(normalIs.outside(), intersectionMapper_.isIndexInInside(nextParallelIs)));
                    this->pairData_[numPairParallelIdx].parallelDistances[1] = nextParallelIs.geometry().volume();
                }
            }

            fillParallelInterpolationFactors_(numPairParallelIdx);

//             if (this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size() == 2)
//             {
//                 this->pairData_[numPairParallelIdx].parallelDistances[1] *= 2.;
//             }
        }

//                     -------------
//            this left|           | parallel dofs also from right,
//  neighbor determines|           | also if statement true once
//if true one or twice,|           | or twice, depending on this
// parallel dofs from  |           | right neighbor
// left                |           |
//                     ======-------
//                     |    ||     |
//                     |    ||     |
//                     |    ||     |
//                     -------------
// ||:myintersection, =: normalIntersection

        else
        {
            //unitNormal
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
                const auto unitNormal = intersection_.centerUnitOuterNormal();
                const auto nextUnitNormal = nextElementIs.centerUnitOuterNormal();

                if ( containerCmp(nextUnitNormal, unitNormal))
                {
                    // can be true once or twice
                    const auto& nextParallelIs = nextElementIs;
                    this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_.globalIntersectionIndex(normalIs.outside(), intersectionMapper_.isIndexInInside(nextParallelIs)));
                    this->pairData_[numPairParallelIdx].parallelDistances[1] = nextParallelIs.geometry().volume();
                }
            }

            fillParallelInterpolationFactors_(numPairParallelIdx, true);

            if (this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size() == 2)
            {
                this->pairData_[numPairParallelIdx].parallelDistances[1] *= 2.;
            }

            unsigned int numDofsOffset = this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size();

            //minusUnitNormal
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
                const auto unitNormal = intersection_.centerUnitOuterNormal();
                auto minusUnitNormal = unitNormal;
                minusUnitNormal *= (-1.0);
                const auto nextUnitNormal = nextElementIs.centerUnitOuterNormal();

                if ( containerCmp(nextUnitNormal, minusUnitNormal))
                {
                    // can be true once or twice
                    const auto& nextParallelIs = nextElementIs;
                    this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_.globalIntersectionIndex(normalIs.outside(), intersectionMapper_.isIndexInInside(nextParallelIs)));
                    this->pairData_[numPairParallelIdx].parallelDistances[1] = nextParallelIs.geometry().volume();
                }
            }
            fillParallelInterpolationFactors_(numPairParallelIdx, true, numDofsOffset);

            if (this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size() - numDofsOffset == 2)
            {
                this->pairData_[numPairParallelIdx].parallelDistances[1] *= 2.;
            }
        }

        numPairParallelIdx++;
    }

    void fillParallelInterpolationFactors_(const int numPairParallelIdx, bool halfed = false, int numDofsOffset = 0)
    {
        //TODO adapt for higher order
        auto& interpFactors = this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0/*without higher order*/];
        auto& parallelDofsFilled = this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/];

        unsigned int numDofs = parallelDofsFilled.size() - numDofsOffset;

        if (numDofs == 1)
        {
            if (halfed == false)
                interpFactors.push_back(1);
            else if (halfed == true)
                interpFactors.push_back(.5);
        }
        else if (numDofs == 2)
        {
            if (halfed == false)
            {
                interpFactors.push_back(.5);
                interpFactors.push_back(.5);
            }
            else if (halfed == true)
            {
                interpFactors.push_back(.25);
                interpFactors.push_back(.25);
            }
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "Did not expect anything other than 1 or 2 parallelDofs-offset.");
        }
    }

    void treatNoParallel_(const Intersection& normalIs, int& numPairParallelIdx)
    {
        this->pairData_[numPairParallelIdx].parallelDofs[0].push_back(-1);
        // If the normalIs has no neighbor we have to deal with the virtual outer parallel dof
        const auto& elementCenter = this->element_.geometry().center();
        const auto& boundaryFacetCenter = normalIs.geometry().center();

        auto distance = boundaryFacetCenter - elementCenter;

        if (scalarCmp(distance[0]*distance[0] + distance[1]*distance[1], intersection_.geometry().volume()*intersection_.geometry().volume()))
        {
//                           /////////////
//          -------------------------------
//          |       |      ||      ^ dis- |
//          |       |      ||      | tance|
//          ----------------|      v      |
//          |       |       |             |
//          |       |       |             |
//          -------------------------------

            distance[0] /= 2.;
            distance[1] /= 2.;
        }

        const auto virtualFirstParallelFaceDofPos = this->intersection_.geometry().center() + distance;

        this->pairData_[numPairParallelIdx].virtualFirstParallelFaceDofPos = std::move(virtualFirstParallelFaceDofPos);

        numPairParallelIdx++;
    }

    bool haveCommonNonDirectionalCornerCoordinate_(const Intersection& isA, const Intersection& isB)
    {
        bool commonCoordinates = false;

        const auto nonDirIndices = nonDirectionIndices();

        if (nonDirIndices.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        for (const auto nonDirectionIdx : nonDirIndices)
        {
            std::vector<Scalar> isACorners;
            for (unsigned int i = 0; i < isA.geometry().corners(); ++i)
            {
                isACorners.push_back(isA.geometry().corner(i)[nonDirectionIdx]);
            }

            for (unsigned int i = 0; i < isB.geometry().corners(); ++i)
            {
                const auto isBCorner = isB.geometry().corner(i)[nonDirectionIdx];

                if (scalarFind_(isACorners.begin(), isACorners.end(), isBCorner) != isACorners.end())
                {
                    commonCoordinates = true;
                }
            }
        }

        return commonCoordinates;
    }

    template<class InputIt, class T>
    constexpr InputIt scalarFind_(InputIt first, InputIt last, const T& value)
    {
        for (; first != last; ++first) {
            if (scalarCmp(*first, value)) {
                return first;
            }
        }
        return last;
    }

// non-directional: not in the direction of the normal vector of intersection_
// true if the non-directional coordinate of one of the corners of intersection_ matches the
// non-directional coordinate of the center of isWithCenter
// is typically used in a case intersection_.inside().level() < intersection_.outside().level()
//                     -------------
//                    |            |
//                    |            |warning:
//                    |            |this opposite intersection also returns true
//                    ||           |if there is a coarse neighbor -> needs to be
//                    ||           |combined with facetIsNormal_ !!!
//                    ||           |
//                     ======-******
//                    |      |     |
//                    |      |     |
//                    |      |     |
//                     -------------
// ||:myintersection, === and ***: isWithCenters which return true

    bool haveNonDirectionCornerCenterMatch_(const Intersection& isWithCenter)
    {
        //TODO figure out in 3D what I want

        bool retVal = false;

        const auto nonDirIndices = nonDirectionIndices();

        if (nonDirIndices.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        for (const auto nonDirectionIdx : nonDirIndices)
        {
            for (unsigned int i = 0; i < intersection_.geometry().corners(); ++i)
            {
                const auto cor = intersection_.geometry().corner(i);

                if (scalarCmp(cor[nonDirectionIdx], isWithCenter.geometry().center()[nonDirectionIdx]))
                {
                    retVal = true;
                }
            }
        }

        return retVal;
    }

    bool haveNonDirectionCenterCornerMatch_(const Intersection& isWithCorners)
    {
        bool retVal = false;

        for (unsigned int i = 0; i < isWithCorners.geometry().corners(); ++i)
        {
            const auto cor = isWithCorners.geometry().corner(i);

            if (scalarCmp(cor[directionIndex()], intersection_.geometry().center()[directionIndex()]))
            {
                retVal = true;
            }
        }

        return retVal;
    }

    Intersection intersection_; //!< The intersection of interest
    const Element element_; //!< The respective element
    const typename Element::Geometry elementGeometry_; //!< Reference to the element geometry
    const GridView gridView_; //!< The grid view
    const IntersectionMapper intersectionMapper_;
    MyAxisData<Scalar> axisData_; //!< Data related to forward and backward faces
    std::array<MyPairData<Scalar, GlobalPosition>, numPairs> pairData_; //!< Collection of pair information related to normal and parallel faces
    std::array<MyVolVarsData<Scalar>, numPairs> volVarsData_;
};

template<class GridView, class IntersectionMapper>
int MyMyFreeFlowStaggeredGeometryHelper<GridView, IntersectionMapper>::order_ = 1;

} // end namespace Dumux

#endif
