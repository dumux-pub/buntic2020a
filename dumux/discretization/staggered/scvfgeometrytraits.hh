// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredSubControlVolumeFace
 */
#ifndef DUMUX_DISCRETIZATION_MY_STAGGERED_SUBCONTROLVOLUMEFACE_TRAITS_HH
#define DUMUX_DISCRETIZATION_MY_STAGGERED_SUBCONTROLVOLUMEFACE_TRAITS_HH

#include <dumux/discretization/staggered/freeflow/mymystaggeredgeometryhelper.hh>

namespace Dumux {
/*!
 * \ingroup StaggeredDiscretization
 * \brief Default traits class to be used for the sub-control volume faces
 *        for the staggered finite volume scheme
 * \tparam GV the type of the grid view
 */
template<class GridView>
struct MyStaggeredDefaultScvfGeometryTraits
{
    using Geometry = typename GridView::template Codim<1>::Geometry;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using LocalIndexType = unsigned int;
    using Scalar = typename GridView::ctype;
    using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimensionworld>;
    using Intersection = typename GridView::Intersection;
    using AxisData = MyAxisData<Scalar>;
    using PairData = MyPairData<Scalar, GlobalPosition>;
    using VolVarsData = MyVolVarsData<Scalar>;
};
} // end namespace Dumux

#endif
